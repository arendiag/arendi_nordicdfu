﻿namespace BleUpdate
{
  public interface IApplicationEnvironment
  {
    string GetVersion();
  }
}