﻿using Arendi.BleLibrary;
using Arendi.BleLibrary.Extention;
using Arendi.BleLibrary.Remote;

namespace BleUpdate.Model
{
  public class UpdatePeripheral : ManagedPeripheral
  {
    public UpdatePeripheral(Uuid uuid) : base(uuid)
    {
    }

    public UpdatePeripheral(IPeripheral peripheral) : base(peripheral)
    {
    }
  }
}

