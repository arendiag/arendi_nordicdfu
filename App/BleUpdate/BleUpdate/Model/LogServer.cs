﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Arendi.DotNETLibrary.Log;

namespace BleUpdate.Model
{
  public abstract class LogServer
  {
    const int serverPort = 8080;

    private static TcpListener listener;
    private static TcpClient client;
    private static NetworkStream stream;
    private static AutoResetEvent clientError;

    public static void Start()
    {
      // enable logging
      LogManager.DefaultLevel = LogLevel.Debug;
      Thread thread = new Thread(DebugThread);
      thread.IsBackground = true;
      thread.Start();
    }

    public static void DebugThread()
    {
      try
      {
        TcpListener server = new TcpListener(IPAddress.Any, serverPort);
        clientError = new AutoResetEvent(false);
        while (true)
        {
          // start listening
          server.Start();

          // wait for connecting client
          client = server.AcceptTcpClient();

          // stop listening for new clients
          server.Stop();

          // get network stream
          stream = client.GetStream();

          // enable logging
          LogManager.LogWritten += LogManagerOnLogWritten;

          // wait until the connection gets closed
          clientError.WaitOne();

          // disable logging
          LogManager.LogWritten -= LogManagerOnLogWritten;

          // clear stream
          stream.Dispose();
          stream = null;

          // close and release client
          client.Close();
          client = null;
        }

      }
      catch (Exception)
      {
        System.Diagnostics.Debug.WriteLine("DEBUG THREAD CRASHED!!!");
      }
    }

    private static void LogManagerOnLogWritten(object sender, LogWrittenEventArgs e)
    {
      // prepare message

      // plain text (open connection with a classic terminal as putty)
      // byte[] message = Encoding.ASCII.GetBytes(e.Entry.ToString() + "\r\n");

      // XML-format (open connection with a log4view viewer (www.log4view.com))
      byte[] message = Encoding.ASCII.GetBytes(string.Format("<log4net:event logger=\"{0}\" timestamp=\"{1}\" level=\"{2}\"><log4net:message>{3}</log4net:message></log4net:event>", ((ILog)sender).Name, e.Entry.Time.ToString("yyyy-MM-dd") + "T" + e.Entry.Time.ToString("hh:mm:ss.fffzzz"), e.Entry.Level.ToString().ToUpper(), e.Entry.Message.ToString()));

      // send message
      try
      {
        // try to send the information
        stream.Write(message, 0, message.Length);
      }
      catch (Exception)
      {
        // connection failure -> disconnect
        clientError.Set();
      }
    }

  }
}
