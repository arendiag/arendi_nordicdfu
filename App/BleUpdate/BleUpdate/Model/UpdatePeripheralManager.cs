﻿using System;
using System.Collections.Generic;
using System.Text;

using Arendi.BleLibrary.Extention;
using Arendi.BleLibrary.Local;
using Arendi.BleLibrary.Remote;
using Arendi.BleLibrary.Utility;

namespace BleUpdate.Model
{
    public class UpdatePeripheralManager : PeripheralManager<UpdatePeripheral>
    {
      public UpdatePeripheral Selected
      {
        get;
        set;
      }

      public UpdatePeripheralManager(ICentral central) : base(central, delegate(IPeripheral peripheral, AdvertiseInfoList list, int arg3) { return new UpdatePeripheral(peripheral); })
      {
        Selected = null;
      }

      /// <summary>
      /// Method called when a peripheral has not been seen (advertised) for the configured <see cref="P:Arendi.BleLibrary.Extention.PeripheralManager{`0}.PeripheralInvisibleTimeout"/>
      ///             time to check if this peripheral can be removed from the list. A derived class may overwrite this method to add other
      ///             criteria's for the check (e.g. Peripheral is currently selected by the application).
      /// </summary>
      /// <param name="peripheral"/>
      /// <returns/>
      protected override bool CheckRemove(UpdatePeripheral peripheral)
      {
        return (peripheral != Selected);
      }
    }
}
