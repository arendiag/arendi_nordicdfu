using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Arendi.BleLibrary;
using Arendi.BleLibrary.Extention;
using Arendi.BleLibrary.Local;
using Arendi.BleLibrary.Service.NordicDFU;
using Arendi.DotNETLibrary.Log;

namespace BleUpdate.Model
{
  public static class Service
  {
    #region Properties
    public static ICentral Central { get; private set; }


    public static UpdatePeripheralManager PeripheralManager;
    public static DfuController UpdateController { get; private set; }
    #endregion

    #region Consts
    #endregion


    #region Members
    //private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    #region Methods
    /// <summary>
    /// Initializes a new instance of the service classes.
    /// </summary>
    public static void Init()
    {
      // enable generic logging
      //LogManager.DefaultLevel = LogLevel.All;

      Central = Arendi.BleLibrary.CentralFactory.GetCentral();

      PeripheralManager = new UpdatePeripheralManager(Central);

      UpdateController = new DfuController();
    }

    #endregion
  }
}

