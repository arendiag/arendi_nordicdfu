﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

namespace BleUpdate.View
{
  public class PeripheralCell : ViewCell
  {
    readonly Label nameLabel;
    readonly Label rssiLabel;

    public PeripheralCell()
    {

      //instantiate each of our views
      StackLayout cellWrapper = new StackLayout();
      StackLayout horizontalLayout = new StackLayout();
      nameLabel = new Label();
      rssiLabel = new Label();

      //set bindings
      nameLabel.SetBinding(Label.TextProperty, "Name");
      rssiLabel.SetBinding(Label.TextProperty, "Rssi");

      //Set properties for desired design
      //cellWrapper.BackgroundColor = Color.FromHex("#eee");
      horizontalLayout.Orientation = StackOrientation.Horizontal;
      horizontalLayout.Padding = Device.OnPlatform(new Thickness(30, 10, 20, 10), new Thickness(10, 10, 20, 10), new Thickness(20, 20, 20, 20));
      rssiLabel.HorizontalOptions = LayoutOptions.EndAndExpand;
      nameLabel.FontAttributes = FontAttributes.Bold;
      rssiLabel.FontAttributes = FontAttributes.Italic;
      //nameLabel.TextColor = Color.FromHex("#f35e20");
      //rssiLabel.TextColor = Color.FromHex("503026");

      //add views to the view hierarchy
      horizontalLayout.Children.Add(nameLabel);
      horizontalLayout.Children.Add(rssiLabel);
      cellWrapper.Children.Add(horizontalLayout);
      View = cellWrapper;
    }
  }
}
