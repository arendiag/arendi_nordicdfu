using System;
using System.Reflection;

using Arendi.BleLibrary.Extention;
using Arendi.DotNETLibrary.Log;

namespace BleUpdate.View
{
  public class PeripheralViewModel<T> : BaseViewModel, IDisposable where T : ManagedPeripheral
  {
    public T Peripheral { get; private set; }

    private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

    public string Name
    {
      get
      {
        string name = Peripheral.Name;
        if (string.IsNullOrEmpty(name))
        {
          return "(no name)";
        }
        return (name);
      }
    }

    public string Rssi
    {
      get
      {
        return (string.Format("{0}dBm", Peripheral.Rssi));
      }
    }

    public PeripheralViewModel(T peripheral)
    {
      Peripheral = peripheral;
      Peripheral.NameUpdated += PeripheralOnNameUpdated;
      Peripheral.RssiUpdated += PeripheralOnRssiUpdated;
    }



    private void PeripheralOnNameUpdated(object sender, NameUpdatedEventArgs e)
    {
      UIThread.InvokedOnMainThread(() =>
      {
        try
        {
          NotifyPropertyChanged("Name");
        }
        catch (Exception ex)
        {
          Log.Warn("Exception on Name notification", ex);
        }
      });
    }

    private void PeripheralOnRssiUpdated(object sender, RssiUpdatedEventArgs e)
    {
      UIThread.InvokedOnMainThread(() =>
      {
        try
        {
          NotifyPropertyChanged("Rssi");
        }
        catch (Exception ex)
        {
          Log.Warn("Exception on Rssi notification", ex);
        }
      });
    }

    public void Dispose()
    {
      Peripheral.NameUpdated -= PeripheralOnNameUpdated;
      Peripheral.RssiUpdated -= PeripheralOnRssiUpdated;
    }
  }
}