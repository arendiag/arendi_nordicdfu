﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace BleUpdate.View
{
    public class TabContentPage : ContentPage
    {
      public TabContentPage()
      {
        Padding = Device.OnPlatform(new Thickness(20, 30, 20, 20), new Thickness(20, 20, 20, 20),
          new Thickness(20, 20, 20, 20));
      }
    internal virtual void Selected()
      {
        
      }

      internal virtual void Deselected()
      {
        
      }

    }
}
