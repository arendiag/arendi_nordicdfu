using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Arendi.BleLibrary.Extention;
using Arendi.BleLibrary.Local;
using Arendi.BleLibrary.Remote;
using BleUpdate.Model;

using Xamarin.Forms;

namespace BleUpdate.View
{
  public class SelectPeripheralPage : ContentPage
  {
    private readonly ObservableCollection<PeripheralViewModel<UpdatePeripheral>> uiPeripheralList;

    public SelectPeripheralPage()
    {
      Title = "Select Peripheral";
      NavigationPage.SetBackButtonTitle(this, "");

      uiPeripheralList = new ObservableCollection<PeripheralViewModel<UpdatePeripheral>>();
      ListView deviceList = new ListView();
      deviceList.ItemsSource = uiPeripheralList;
      deviceList.ItemSelected += OnItemSelected;
      DataTemplate dataTemplate = new DataTemplate(typeof(PeripheralCell));
      deviceList.ItemTemplate = dataTemplate;

      Content = new StackLayout
      {
        VerticalOptions = LayoutOptions.FillAndExpand,
        Children = { deviceList }
      };
    }

    void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
    {
      try
      {
        Service.PeripheralManager.Selected = ((PeripheralViewModel<UpdatePeripheral>)e.SelectedItem).Peripheral;
      }
      catch (Exception)
      {
        return;
      }
      Navigation.PopAsync();

      #if __ANDROID__
      /* Workaround for issue "Xamarin Forms Android OnAppearing Not Triggering After PopModalAsync" (see http://stackoverflow.com/questions/24998149/xamarin-forms-android-onappearing-not-triggering-after-popmodalasync) */
      MessagingCenter.Send(this, "popped");
      #endif
    }

    #region Methods

    private void UpdateList()
    {
      lock (this)
      {
        IEnumerable<UpdatePeripheral> realPeripheralList = Service.PeripheralManager.Peripherals;

        // select items to remove
        List<PeripheralViewModel<UpdatePeripheral>> itemsToRemove = new List<PeripheralViewModel<UpdatePeripheral>>();
        foreach (PeripheralViewModel<UpdatePeripheral> item in uiPeripheralList)
        {
          if (!realPeripheralList.Contains(item.Peripheral))
          {
            itemsToRemove.Add(item);
          }
        }

        // remove items
        foreach (PeripheralViewModel<UpdatePeripheral> item in itemsToRemove)
        {
          item.Dispose();
          uiPeripheralList.Remove(item);
        }

        // add new items
        foreach (UpdatePeripheral item in realPeripheralList)
        {
          bool found = false;
          foreach (PeripheralViewModel<UpdatePeripheral> peripheralViewModel in uiPeripheralList)
          {
            if (peripheralViewModel.Peripheral == item)
            {
              found = true;
              break;
            }
          }

          if (!found)
          {
            uiPeripheralList.Add(new PeripheralViewModel<UpdatePeripheral>(item));
          }
        }
      }
    }

    private void ClearList()
    {
      lock (this)
      {
        // dispose items
        foreach (PeripheralViewModel<UpdatePeripheral> item in uiPeripheralList)
        {
          item.Dispose();
        }
        uiPeripheralList.Clear();
      }

    }

    protected override void OnAppearing()
    {
      base.OnAppearing ();

      // add to added/removed events
      Service.PeripheralManager.PeripheralAdded += OnPeripheralAdded;
      Service.PeripheralManager.PeripheralRemoved += OnPeripheralRemoved;
      Service.PeripheralManager.StartScan();

      // update initial list
      UpdateList();
    }


    protected override void OnDisappearing()
    {
      Service.PeripheralManager.StopScan();
      // remove from added/removed events
      Service.PeripheralManager.PeripheralAdded -= OnPeripheralAdded;
      Service.PeripheralManager.PeripheralRemoved -= OnPeripheralRemoved;

      ClearList();

      base.OnDisappearing ();
    }

    private void OnPeripheralRemoved(object sender, PeripheralRemovedEventArgs<UpdatePeripheral> e)
    {
      UIThread.InvokedOnMainThread(() =>
        {
          UpdateList();
        });
    }

    private void OnPeripheralAdded(object sender, PeripheralAddedEventArgs<UpdatePeripheral> e)
    {
      UIThread.InvokedOnMainThread(() =>
        {
          UpdateList();
        });

    }
    #endregion
  }
}

