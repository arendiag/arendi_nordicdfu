using System;
using System.IO;
using System.Reflection;

using BleUpdate.Model;
using Xamarin.Forms;

using Arendi.BleLibrary.Local;
using Arendi.BleLibrary.Service.NordicDFU;
using Arendi.DotNETLibrary.Log;

namespace BleUpdate.View
{
  public class MainPage : TabContentPage
  {
    private static readonly ILog Log = LogManager.GetLogger("MainPage");

    private const int fontSize = 15;

    readonly Button peripheralButton;

    readonly Button startButton;

    readonly ProgressBar progressBar;
    readonly Label stateLabel;

    private readonly DfuOptions options;
    private bool updateInProgress;

    public MainPage()
    {
      // set title of the page
      Title = "Nordic DFU Demo";

      updateInProgress = false;

      var deviceLabel = new Label {
        Text = "Device",
        HorizontalOptions = LayoutOptions.Fill,
        VerticalOptions = LayoutOptions.CenterAndExpand,
        FontSize = fontSize,
      };

      peripheralButton = new Button {
        Text = "select peripheral",
        HorizontalOptions = LayoutOptions.Fill,
        VerticalOptions = LayoutOptions.CenterAndExpand,
        BorderWidth = Device.OnPlatform(0.5, 0, 0),
      };

        
      var settingsGrid = new Grid();
      settingsGrid.Children.Add(deviceLabel, 0, 0);
      settingsGrid.Children.Add(peripheralButton, 1, 0);
      settingsGrid.RowDefinitions = new RowDefinitionCollection
      { 
          new RowDefinition { Height = 40, },
      };
      settingsGrid.ColumnDefinitions = new ColumnDefinitionCollection
      { 
        new ColumnDefinition { Width = 100, },
        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star), },
      };

      startButton = new Button {
        Text = "Start Update",
        VerticalOptions = LayoutOptions.End,
        HorizontalOptions = LayoutOptions.FillAndExpand,
        BorderColor = Color.Black,
        FontSize = 25,
      };

      progressBar = new ProgressBar
      {
        Progress = 0.0,
        IsVisible = false,
      };

      stateLabel = new Label
      {
        Text = "",
        HorizontalOptions = LayoutOptions.Center,
      };


      // top stack layout
      StackLayout topStackLayout = new StackLayout
      {
        VerticalOptions = LayoutOptions.StartAndExpand,
        Padding = new Thickness(20),
        Spacing = 10,
        Children = {
          settingsGrid,
          new StackLayout
          {
            HeightRequest = 20,
          },
          stateLabel,
          progressBar,
          new StackLayout
          {
            HeightRequest = 20,
          },
          startButton,
        }
      };

      // set content
      Content = topStackLayout;

      peripheralButton.Clicked += (object sender, EventArgs e) => 
      {
        Navigation.PushAsync(new SelectPeripheralPage());
      };

      startButton.Clicked += OnUpdateClicked;

      options = new DfuOptions()
      {
        DelayAfterInitialConnect = Device.OnPlatform(0, 6000, 0),
        PacketReceiveNotificationCount = 25,
      };
    }
      
    protected override void OnAppearing()
    {
      // attach to state change
      Service.Central.BluetoothStateChanged += Ble_Instance_Central_BluetoothStateChanged;

      // check initial state
      if (Service.Central.BluetoothState == BluetoothState.Off)
      {
        ShowBluetoothOffWarning();
      }

      // setup screen
      peripheralButton.Text = (Service.PeripheralManager.Selected == null) ? "select peripheral" : Service.PeripheralManager.Selected.Name;
      SetButtonVisibility();

      Service.UpdateController.Completed += HandleCompleted;
      Service.UpdateController.Progress += HandleProgress;

      base.OnAppearing ();
    }

    protected override void OnDisappearing()
    {
      // detach from state change
      Service.Central.BluetoothStateChanged -= Ble_Instance_Central_BluetoothStateChanged;
      Service.UpdateController.Completed -= HandleCompleted;
      Service.UpdateController.Progress -= HandleProgress;

      // call base class
      base.OnDisappearing();
    }

    void Ble_Instance_Central_BluetoothStateChanged (object sender, Arendi.BleLibrary.Local.BluetoothStateChangedEventArgs e)
    {
      UIThread.InvokedOnMainThread(() =>
      {
        // bluetooth disabled ?
        if (e.NewStatus == BluetoothState.Off)
        {
          ShowBluetoothOffWarning();
        }
      });
    }

    void ShowBluetoothOffWarning()
    {
      DisplayAlert("BLE Update", "Bluetooth is disabled!", "OK");
    }

    void SetButtonVisibility()
    {
      if (updateInProgress)
      {
        startButton.IsEnabled = false;
        peripheralButton.IsEnabled = false;
        return;
      }
      peripheralButton.IsEnabled = true;
      startButton.IsEnabled = (Service.PeripheralManager.Selected != null);
    }

    #region Update

    private void OnUpdateClicked(object sender, EventArgs e)
    {
      try
      {
        updateInProgress = true;
        SetButtonVisibility();
        progressBar.IsVisible = true;

        Stream hexFileStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("BleUpdate.nrf51422_xxac_s130.hex");
        if (hexFileStream != null)
        {
          byte[] data = new byte[hexFileStream.Length];
          hexFileStream.Read(data, 0, data.Length);
          DfuFirmwareImage dfuFirmwareImage = new DfuFirmwareImage(data);

          Service.UpdateController.Update(Service.PeripheralManager.Selected.Peripheral,
                                          null,
                                          null,
                                          dfuFirmwareImage.Data,
                                          options);
        }
        else
        {
          Log.Error("Failed to load hex file from ressources");
        }
      }
      catch (Exception ex)
      {
        updateInProgress = false;
        Log.Error("Failed to start update", ex);
      }
    }
    string GetStatusText(DfuStatus status)
    {
      switch (status)
      {
        case DfuStatus.ConnectingDevice:
          return ("Connecting to device");

        case DfuStatus.DiscoverServices:
          return ("Discover services of the device");

        case DfuStatus.PrepareService:
          return ("Prepare service for update");

        case DfuStatus.StartUpdate:
          return ("Enter DFU mode");

        case DfuStatus.ReadDfuRevision:
          return ("Read DFU revision");

        case DfuStatus.InitializeUpdateParameters:
          return ("Set DFU parameters");

        case DfuStatus.TransmitSoftdevice:
          return ("Transfer softdevice");

        case DfuStatus.TransmitBootloader:
          return ("Transfer bootloader");

        case DfuStatus.TransmitApplication:
          return ("Transfer application");

        case DfuStatus.ValidateFirmware:
          return ("Validate firmware");

        case DfuStatus.ActivateImageAndReset:
          return ("Activate firmware and reset");

        case DfuStatus.ConnectDelay:
          return ("Connect Delay");

        case DfuStatus.Idle:
          return ("Idle");

        default:
          return status.ToString();
      }
    }


    string GetFailureText(DfuFailure failure)
    {
      switch (failure)
      {
        case DfuFailure.CharacteristicNotFound:
          return ("DFU characteristic not found");

        case DfuFailure.CharacteristicNotificationFailure:
          return ("Notification for DFU characteristic couldn't be configured");

        case DfuFailure.CrcError:
          return ("CRC check failure");

        case DfuFailure.DataSizeExceedsLimit:
          return ("Data size of the firmware exceeds the limit");

        case DfuFailure.Exception:
          return ("Unexpected exception");

        case DfuFailure.InvalidParameter:
          return ("Invalid parameter for update");

        case DfuFailure.InvalidResponse:
          return ("Invalid response received");

        case DfuFailure.InvalidState:
          return ("Device is in invalid state");

        case DfuFailure.MissingAcknowledged:
          return ("No acknowledge received from device");

        case DfuFailure.NotSupported:
          return ("Not supported by device");

        case DfuFailure.OperationFailed:
          return ("Operation failed");

        case DfuFailure.ServiceDiscoveryFailed:
          return ("Service discovery failed");

        case DfuFailure.NoCompatibleServiceFound:
          return ("No compatible DFU service found");

        case DfuFailure.UnableToConnect:
          return ("Unable to connect device");

        case DfuFailure.UnableToDisconnect:
          return ("Unable to disconnect device");

        case DfuFailure.ReadFailed:
          return ("Read data failure");

        case DfuFailure.WriteFailed:
          return ("Write data failure");

        default:
          return failure.ToString();
      }
    }



    void HandleCompleted(object sender, DfuCompletedEventArgs e)
    {
      UIThread.InvokedOnMainThread(() => {
        string completedText;
        if (e.Success)
        {
          completedText = string.Format("Update Success!");
        }
        else
        {
          completedText = string.Format("Update Failed! ({0})", GetFailureText(e.Failure));
        }
        //logContent.Text += string.Format("{0}{1}", Environment.NewLine, completedText);
        stateLabel.Text = completedText;
        updateInProgress = false;
        SetButtonVisibility();
      });
    }



    void HandleProgress(object sender, DfuProgressEventArgs e)
    {
      UIThread.InvokedOnMainThread(() =>
      {
        if (e.Total == 0)
        {
          progressBar.Progress = 0;
          stateLabel.Text = string.Format("{0}", GetStatusText(e.Status));
        }
        else
        {
          if (e.Index == 0)
          {
            progressBar.Progress = 0;
          }
          else
          {
            progressBar.ProgressTo((double)e.Index / e.Total, 500, Easing.Linear);
          }
          int progressPercent = Math.Min(100, (int)(((double)e.Index / e.Total) * 100));
          stateLabel.Text = string.Format("{0} ({1}%)", GetStatusText(e.Status), progressPercent);
        }

        //logContent.Text += string.Format ("{0}Progress: {1}/{2}", Environment.NewLine, e.Index, e.Total);
      });
    }

    #endregion

  }
}

