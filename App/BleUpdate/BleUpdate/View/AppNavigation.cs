﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Arendi.BleLibrary.Local;
using BleUpdate.Model;
using Xamarin.Forms;

namespace BleUpdate.View
{
  class AppNavigation : TabbedPage
  {
    private MainPage mainPage;
    private InfoPage infoPage;
    private TabContentPage currentPage;

    public AppNavigation()
    {
      this.Title = "Nordic DFU Demo";
      mainPage = new MainPage {Icon = "ic_update_outline.png", };
      infoPage = new InfoPage {Icon = "ic_info_outline.png"};
      this.Children.Add(mainPage);
      this.Children.Add(infoPage);
      this.CurrentPage = mainPage;
      this.PropertyChanged += OnPropertyChanged;
      currentPage = mainPage;
      currentPage.Selected();
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName.Equals("CurrentPage"))
      {
        currentPage.Deselected();
        if(this.CurrentPage == mainPage)
        {
          currentPage = mainPage;
        }
        else if (this.CurrentPage == infoPage)
        {
          currentPage = infoPage;
        }
        currentPage.Selected();
      }
    }

    internal void NavigateToMainPage()
    {
      if (mainPage == null)
      {
        return;
      }
      this.CurrentPage = mainPage;
    }
  }
}
