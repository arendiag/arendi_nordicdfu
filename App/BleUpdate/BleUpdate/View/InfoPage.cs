﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace BleUpdate.View
{
  class InfoPage : TabContentPage
  {

    public InfoPage()
    {
      Title = "Info";

      var softwareVersionText = CreateValueLabel("App version " + App.Version);

      string ct = @"Arendi is a leading supplier of sophisticated Bluetooth Low Energy solutions.

We not only can offer the integration of Bluetooth Low Energy in new or existing devices but are also able to offer the specification of needed services and profiles.

Most Bluetooth applications are also in need of an application on a smart phone, tablet or a PC.With our special knowhow in Requirements Engineering, GUI Definition and Agile Development, we are also able to provide those applications.

The Arendi BLE Library™ offers an easy and simple start in the world of Bluetooth Low Energy. By using the library your engineers don’t have to care about the differences of the various target platforms. They can rely on a unified and well documented interface and benefit from predefined automatisms.

Technologies:
  - Xamarin™ C# for app development
  - .NET for PC applications
  - Bluetooth LE
  - Bluetooth Classic
  - Internet connection / IoT
  - GUI development 
  - Usability, Wireframes, Screendesign

Platforms:
  - Android
  - iOS
  - Windows Phone

For more information don’t hesitate to contact our CTO thomas.rupp@arendi.ch";

      var companyText = CreateMultilineLabel(ct);

      Content = new ScrollView
      {
        Content = new StackLayout
        {
          Padding = new Thickness(0, 10, 0, 0),
          Spacing = 30,
          Children =
          {
            new Image
            {
              Source = "arendi_logo.png",
              HorizontalOptions = LayoutOptions.End,
              VerticalOptions = LayoutOptions.End,
              WidthRequest = Math.Round((double) App.ScreenWidth/2),
            },
            companyText,
            softwareVersionText,
          }
        },
      };
    }


    private Label CreateValueLabel(string text)
    {
      return new Label
      {
        Text = text,
      };
    }

    private Label CreateMultilineLabel(string text)
    {
      return new Label
      {
        LineBreakMode = LineBreakMode.WordWrap,
        Text = text,
      };
    }
  }
}
