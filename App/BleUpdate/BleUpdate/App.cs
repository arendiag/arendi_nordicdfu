using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BleUpdate.Model;
using BleUpdate.View;
using Xamarin.Forms;

namespace BleUpdate
{
  public class App : Application
  {
    // see http://stackoverflow.com/questions/24465513/how-to-get-detect-screen-size-in-xamarin-forms
    public static int ScreenWidth;
    public static int ScreenHeight;
    public static string Version;

    public App ()
    {
      // init services
      Service.Init();

      // The root page of your application
      MainPage = new NavigationPage(new AppNavigation());
    }
  }
}
