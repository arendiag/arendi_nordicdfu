namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Enumeration of the failure that could occur in the DFU.
  /// </summary>
  public enum DfuFailure
  {
#pragma warning disable 1591
    None,
    UnableToConnect,
    UnableToDisconnect,
    ServiceDiscoveryFailed,
    NoCompatibleServiceFound,
    CharacteristicNotFound,
    CharacteristicNotificationFailure,
    ReadFailed,
    WriteFailed,
    Exception,
    MissingAcknowledged,
    InvalidResponse,
    InvalidParameter,
    InvalidState,
    NotSupported,
    DataSizeExceedsLimit,
    CrcError,
    OperationFailed,
    UnknownError,
#pragma warning restore 1591
  }
}

