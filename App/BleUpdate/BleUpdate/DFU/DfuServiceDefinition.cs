namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Service definition class.
  /// </summary>
  internal class DfuServiceDefinition
  {
    #region Constants
    public const string ServiceUuidNordicDfu = "00001530-1212-efde-1523-785feabcd123";
    public const string ControlPointUuidNordicDfu = "00001531-1212-efde-1523-785feabcd123";
    public const string PacketUuidNordicDfu = "00001532-1212-efde-1523-785feabcd123";
    public const string RevisionUuidNordicDfu = "00001534-1212-efde-1523-785feabcd123";
    
    #endregion

    #region Properties
    /// <summary>
    /// Get/Set the service UUID.
    /// </summary>
    public string ServiceUuid { get; set; }

    /// <summary>
    /// Get/Set the control point UUID.
    /// </summary>
    public string ControlPointUuid { get; set; }

    /// <summary>
    /// Get/Set the packet UUID.
    /// </summary>
    public string PacketUuid { get; set; }

    /// <summary>
    /// Get/Set the revision UUID.
    /// </summary>
    public string RevisionUuid { get; set; }
    #endregion

    #region Methods
    /// <summary>
    /// Create DFU service definition (Nordic UUID's).
    /// </summary>
    public DfuServiceDefinition()
    {
      ServiceUuid = ServiceUuidNordicDfu;
      ControlPointUuid = ControlPointUuidNordicDfu;
      PacketUuid = PacketUuidNordicDfu;
      RevisionUuid = RevisionUuidNordicDfu;
    }

    /// <summary>
    /// Create DFU service definition (Other UUID's).
    /// </summary>
    public DfuServiceDefinition(string serviceUuid, string controlPointUuid, string packetUuid, string revisionUuid = null)
    {
      ServiceUuid = serviceUuid;
      ControlPointUuid = controlPointUuid;
      PacketUuid = packetUuid;
      RevisionUuid = revisionUuid;
    }
    #endregion
  }
}
