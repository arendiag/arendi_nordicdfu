﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Arendi.DotNETLibrary;

namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Class representing a DFU firmware image.
  /// </summary>
  public class DfuFirmwareImage
  {
    #region Class
    /// <summary>
    /// Class representing a line of the HEX file.
    /// </summary>
    internal class HexFileLine
    {
      internal enum RecordType
      {
        DataRecord,
        EndOfFileRecord,
        ExtendedSegmentAddressRecord,
        StartSegmentAddressRecord,
        ExtendedLinearAddressRecord,
        StartLinearAddressRecord,
      }

      /// <summary>
      /// Number of data bytes in this record.
      /// </summary>
      internal int NumBytes { get; private set; }

      /// <summary>
      /// Type of the record.
      /// </summary>
      internal RecordType Type { get; private set; }

      /// <summary>
      /// Target address of this record.
      /// </summary>
      internal UInt16 Address { get; private set; }

      /// <summary>
      /// Extended address.
      /// </summary>
      internal UInt32 ExtendedAddress { get; private set; }

      /// <summary>
      /// Record data.
      /// </summary>
      internal byte[] Data { get; private set; }

      /// <summary>
      /// Create a HEX file line object.
      /// </summary>
      /// <param name="line"></param>
      internal HexFileLine(string line)
      {
        if (line[0] != ':') throw new Exception("line start != ':'");

        // convert to binary data
        byte[] data = BinaryString.GetBytes(line);

        // check for minimal length
        if (data.Length < 5) throw new Exception("line is too short");

        // check CRC
        byte crc = 0;
        for (int i = 0; i < (data.Length - 1); i++)
        {
          crc += data[i];
        }
        crc = (byte)((((byte)crc) ^ 0xFF) + 1);
        if (crc != data[(data.Length - 1)]) throw new Exception("invalid checksum");

        // Get number of bytes
        NumBytes = data[0];

        // Read address of the record
        Address = BufferAccess.ReadUInt16BigEndian(data, 1);

        // evaluate the record type
        switch (data[3])
        {
          case 0:
            Type = RecordType.DataRecord;

            // read data
            Data = new byte[NumBytes];
            for (int i = 0; i < NumBytes; i++)
            {
              Data[i] = data[4 + i];
            }
            break;

          case 1:
            Type = RecordType.EndOfFileRecord;
            break;

          case 2:
            Type = RecordType.ExtendedSegmentAddressRecord;
            ExtendedAddress = ((UInt32)BufferAccess.ReadUInt16BigEndian(data, 4)) * 16;
            break;

          case 3:
            Type = RecordType.StartSegmentAddressRecord;
            break;

          case 4:
            Type = RecordType.ExtendedLinearAddressRecord;
            ExtendedAddress = (((UInt32)BufferAccess.ReadUInt16BigEndian(data, 4)) << 16);
            break;

          case 5:
            Type = RecordType.StartLinearAddressRecord;
            break;

          default:
            throw new Exception("Unknown record type");
        }
      }
    }
    #endregion

    #region Properties
    /// <summary>
    /// Type of the firmware
    /// </summary>
    public DfuFirmwareImageType Type
    {
      get;
      private set;
    }

    /// <summary>
    /// Data of the firmware
    /// </summary>
    public byte[] Data
    {
      get; private set;
    }

    /// <summary>
    /// First address of the firmware
    /// </summary>
    public UInt32 Address
    {
      get;
      private set;
    }

    /// <summary>
    /// Size of the firmware
    /// </summary>
    public UInt32 Size
    {
      get;
      private set;
    }
    #endregion

    #region Methods
    /// <summary>
    /// Setup firmware image from binary data
    /// </summary>
    /// <param name="data">Binary content of a HEX file containing a softdevice, a bootloader or an application.</param>
    public DfuFirmwareImage(byte[] data)
    {
      string hexFile = Encoding.ASCII.GetString(data);
      string[] hexLinesText = hexFile.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
      var hexLines = hexLinesText.Select(hexLineTest => new HexFileLine(hexLineTest)).ToList();

      // extract address and size from hex lines
      ExtractAddressAndSize(hexLines);

      // extract data from hex lines
      ExtractData(hexLines);

      // determine type
      if (Address == 0)
      {
        Type = DfuFirmwareImageType.Softdevice;
      }
      else if (Address <= 0x30000)
      {
        Type = DfuFirmwareImageType.Application;
      }
      else
      {
        Type = DfuFirmwareImageType.Bootloader;
      }
    }

    private void ExtractAddressAndSize(IEnumerable<HexFileLine> hexLines)
    {
      UInt32 startAddress = UInt32.MaxValue;
      UInt32 endAddress = UInt32.MinValue;

      UInt32 extendedAddress = 0;

      foreach (HexFileLine hexLine in hexLines)
      {
        switch (hexLine.Type)
        {
          case HexFileLine.RecordType.DataRecord:
            UInt32 lineStartAddress = ((UInt32)extendedAddress) + hexLine.Address;
            UInt32 lineEndAddress = (UInt32)(lineStartAddress + hexLine.NumBytes - 1);

            /*
               Check if data is within normal flash range. The HEX file also contains the values
               for the UICR registers. Date for other ranges is ignored.
            */
            if (lineEndAddress <= 0x7FFFF)
            {
              startAddress = Math.Min(startAddress, lineStartAddress);
              endAddress = Math.Max(endAddress, lineEndAddress);
            }
            break;

          case HexFileLine.RecordType.EndOfFileRecord:
            continue;

          case HexFileLine.RecordType.ExtendedSegmentAddressRecord:
            extendedAddress = hexLine.ExtendedAddress;
            break;

          case HexFileLine.RecordType.StartSegmentAddressRecord:
            break;

          case HexFileLine.RecordType.ExtendedLinearAddressRecord:
            extendedAddress = hexLine.ExtendedAddress;
            break;

          case HexFileLine.RecordType.StartLinearAddressRecord:
            break;

          default:
            throw new ArgumentOutOfRangeException();
        }
      }

      Address = startAddress;
      Size = (endAddress - startAddress) + 1;
    }


    private void ExtractData(IEnumerable<HexFileLine> hexLines)
    {
      UInt32 extendedAddress = 0;

      Data = new byte[Size];
      for (int i = 0; i < Size; i++) Data[i] = 0xFF;

      foreach (HexFileLine hexLine in hexLines)
      {
        switch (hexLine.Type)
        {
          case HexFileLine.RecordType.DataRecord:
            UInt32 lineStartAddress = ((UInt32)extendedAddress) + hexLine.Address;
            UInt32 lineEndAddress = (UInt32)(lineStartAddress + hexLine.NumBytes - 1);
            if ((lineStartAddress >= Address) && (lineEndAddress < (Address + Size)))
            {
              Array.Copy(hexLine.Data, 0, Data, lineStartAddress - Address, hexLine.NumBytes);
            }
            break;

          case HexFileLine.RecordType.EndOfFileRecord:
            continue;

          case HexFileLine.RecordType.ExtendedSegmentAddressRecord:
            extendedAddress = hexLine.ExtendedAddress;
            break;

          case HexFileLine.RecordType.StartSegmentAddressRecord:
            break;

          case HexFileLine.RecordType.ExtendedLinearAddressRecord:
            extendedAddress = hexLine.ExtendedAddress;
            break;

          case HexFileLine.RecordType.StartLinearAddressRecord:
            break;

          default:
            throw new ArgumentOutOfRangeException();
        }
      }
    }
    #endregion

  }
}
