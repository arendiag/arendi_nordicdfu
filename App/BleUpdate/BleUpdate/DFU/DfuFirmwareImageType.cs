namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Enumeration of the DFU image types.
  /// </summary>
  public enum DfuFirmwareImageType
  {
#pragma warning disable 1591
    Softdevice,
    Bootloader,
    Application,
#pragma warning restore 1591
  }
}

