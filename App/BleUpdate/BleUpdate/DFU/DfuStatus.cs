namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Enumeration of the DFU status.
  /// </summary>
  public enum DfuStatus
  {
#pragma warning disable 1591
    Idle,                         // not used in DFU itself, but useful on upper layers
    ConnectingDevice,
    ConnectDelay,
    DiscoverServices,
    PrepareService,
    StartUpdate,
    ReadDfuRevision,
    InitializeUpdateParameters,
    TransmitSoftdevice,
    TransmitBootloader,
    TransmitApplication,
    ValidateFirmware,
    ActivateImageAndReset,
#pragma warning restore 1591
  }
}

