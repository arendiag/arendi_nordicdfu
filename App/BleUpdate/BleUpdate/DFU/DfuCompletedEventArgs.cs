using System;

namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Event arguments for the event <see cref="DfuController.Completed"/>.
  /// </summary>
  public class DfuCompletedEventArgs : EventArgs
  {
    /// <summary>
    /// Get the failure that aborted the update. If no failure occurred the failure will be <see cref="DfuFailure.None"/>.
    /// </summary>
    public DfuFailure Failure
    {
      get;
      private set;
    }

    /// <summary>
    /// Was the update a success?
    /// </summary>
    public bool Success
    {
      get
      {
        return (Failure == DfuFailure.None);
      }
    }

    /// <summary>
    /// Create the DFU completed event arguments.
    /// </summary>
    /// <param name="failure">Failure the ended the update.</param>
    public DfuCompletedEventArgs(DfuFailure failure)
    {
      Failure = failure;
    }

    /// <summary>
    /// Create the DFU completed event arguments.
    /// </summary>
    public DfuCompletedEventArgs()
    {
      Failure = DfuFailure.None;
    }
  }
}

