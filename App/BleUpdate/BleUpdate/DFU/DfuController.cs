using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Arendi.BleLibrary.Remote;
using Arendi.DotNETLibrary;
using Arendi.DotNETLibrary.Log;

namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Controller instance of the DFU process.
  /// </summary>
  public class DfuController
  {
    #region Events
    /// <summary>
    /// Event triggered to indicate the progress of the DFU.
    /// </summary>
    public event EventHandler<DfuProgressEventArgs> Progress;

    /// <summary>
    /// Event triggered to indicate the end of the DFU.
    /// </summary>
    public event EventHandler<DfuCompletedEventArgs> Completed;
    #endregion

    #region Properties
    #endregion

    #region Constants
#if ANDROID
    // ANDROID sometimes has long delays in reconnect a device!
    const int connectTimeout = 30000;
#else
    const int connectTimeout = 5000;
#endif
    const int disconnectTimeout = 5000;
    const int serviceDiscoverTimeout = 10000;
    const int notificationChangeTimeout = 3000;
    const int readDataTimeout = 3000;
    const int writeDataTimeout = 3000;
    const int startDfuTimeout = 8000;
    const int startDfuNextAttemptDelay = 1000;
    const int connectNextAttemptDelay = 1000;
    const int initializeDfuParametersTimeout = 3000;
    const int packetReceiptNotificationTimeout = 2000;
    const int receiveFirmwareResponseTimeout = 2000;
    const int validateFirmwareTimeout = 5000;
    const int activateImageAndResetTimeout = 5000;

    const int TransmitFirmwareProgressEventInterval = 200;
    const int TransmitFirmwarePacketSize = 20;

    const byte opCodeStartDfu                           = 0x01;
    const byte opCodeInitializeDfuParameters            = 0x02;
    const byte opCodeReceiveFirmwareImage               = 0x03;
    const byte opCodeValidateFirmware                   = 0x04;
    const byte opCodeActivateImageAndReset              = 0x05;
    const byte opCodePacketReceiptNotificationRequest   = 0x08;
    const byte opCodeResponseCode                       = 0x10;
    const byte opCodePacketReceiptNotification          = 0x11;

    const byte responseValueSuccess                     = 0x01;
    const byte responseValueInvalidState                = 0x02;
    const byte responseValueNotSupported                = 0x03;
    const byte responseValueDataSizeExceedsLimit        = 0x04;
    const byte responseValueCrcError                    = 0x05;
    const byte responseValueOperationFailed             = 0x06;
    #endregion

    #region Members
    Thread updateThread;
    private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    #region Methods
    /// <summary>
    /// Initializes a new instance of the <see cref="DfuController"/> class.
    /// </summary>
    public DfuController()
    {
      // enable debugging
#if DEBUG
      Log.Level = LogLevel.All;
#endif
    }

    /// <summary>
    /// Connect the specified peripheral.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    async Task Connect(IPeripheral peripheral)
    {
      Log.Info("Connect device...");
      await peripheral.ConnectAsync(connectTimeout);
      Log.Info("Connected");
    }


    /// <summary>
    /// Disconnect the specified peripheral.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    static async Task Disconnect(IPeripheral peripheral)
    {
      // peripheral is connected?
      if (peripheral.IsConnected)
      {
        Log.Info("Disconnect device...");
        await peripheral.DisconnectAsync(disconnectTimeout);
        Log.Info ("Disconnected");
      }
    }


    /// <summary>
    /// Discovers the services.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
     async Task DiscoverServices(IPeripheral peripheral)
    {
      Log.Info("Discover services...");
      await peripheral.DiscoverServicesAsync(serviceDiscoverTimeout);
      Log.Info("Services discovered");
    }


    /// <summary>
    /// Updates the notification.
    /// </summary>
    /// <returns>The notification.</returns>
    /// <param name="characteristic">Characteristic.</param>
    /// <param name="enable">If set to <c>true</c> enable.</param>
    static async Task UpdateNotification(ICharacteristic characteristic, bool enable)
    {
      Log.Info("Enable notifications...");
      await characteristic.ChangeNotificationAsync(enable, notificationChangeTimeout);
      Log.Info("Notifications enabled");
    }


    /// <summary>
    /// Reads the data.
    /// </summary>
    /// <param name="characteristic">Characteristic.</param>
    /// <param name="logEnable">If set to <c>true</c> log enable.</param>
    /// <param name="retries">Retries.</param>
    static async Task<byte[]> ReadData(ICharacteristic characteristic, bool logEnable = true, int retries = 0)
    {
      byte[] data = null;

      int retry = 0;
      do
      {
        if (logEnable)
        {
          if (retry == 0)
          {
            Log.Debug("Read data... ");
          }
          else
          {
            Log.DebugFormat("Read data... Retry #{0}", retry);
          }
        }

        try
        {
          data = await characteristic.ReadDataAsync(readDataTimeout);
        }
        catch (Exception)
        {
          if (retry >= retries) throw;
          retry++;
        }
      } while (data == null);

      if (logEnable) Log.InfoFormat("Data read ({0})", BinaryString.ToString(data));
      return (data);
    }


    /// <summary>
    /// Writes the data.
    /// </summary>
    /// <returns>The data.</returns>
    /// <param name="characteristic">Characteristic.</param>
    /// <param name="data">Data.</param>
    /// <param name="logEnable"></param>
    /// <param name="retries"></param>
    static async Task WriteData(ICharacteristic characteristic, byte[] data, bool logEnable = true, int retries = 0)
    {
      int retry = 0;
      bool dataWritten = false;

      do
      {
        if (logEnable)
        {
          if (retry == 0)
          {
            Log.DebugFormat("Write data... ({0})", BinaryString.ToString(data));
          }
          else
          {
            Log.DebugFormat("Write data... ({0}) Retry #{1}", BinaryString.ToString(data), retry);
          }
        }

        try
        {
          await characteristic.WriteDataAsync(data, writeDataTimeout);
          dataWritten = true;
        }
        catch (Exception)
        {
          if (retry >= retries) throw;
          retry++;
        }
      } while (!dataWritten);


      // success
      if (logEnable) Log.Info("Data written");
    }


    /// <summary>
    /// Starts the DFU.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicPacket">DFU characteristic packet.</param>
    /// <param name="dfuCharacteristicControlPoint">DFU characteristic control point.</param>
    /// <param name="softdevice">Softdevice.</param>
    /// <param name="bootloader">Bootloader.</param>
    /// <param name="application">Application.</param>
    async Task StartDFU(IPeripheral peripheral, ICharacteristic dfuCharacteristicPacket, ICharacteristic dfuCharacteristicControlPoint, byte[] softdevice, byte[] bootloader, byte[] application)
    {
      byte responseValue = 0;

      AutoResetEvent responseReceviedEvent = new AutoResetEvent(false);
      AutoResetEvent disconnectedEvent = new AutoResetEvent(false);


      EventHandler<NotificationReceivedEventArgs> notificationReceviedHandler = delegate(object sender, NotificationReceivedEventArgs e)
      {
        byte[] data = e.Data;
        if (data == null) return;

        if (   (data.Length >= 3)
          && (data[0] == opCodeResponseCode)
          && (data[1] == opCodeStartDfu))
        {
          responseValue = data[2];
          Log.InfoFormat("Start DFU response ({0})", responseValue);
          responseReceviedEvent.Set();
        }
      };

      EventHandler<DisconnectedEventArgs> disconnectedHandler = delegate(object sender, DisconnectedEventArgs e)
      {
        disconnectedEvent.Set();
      };

      Log.Debug ("Start DFU...");

      try
      {
        dfuCharacteristicControlPoint.NotificationReceived += notificationReceviedHandler;
        peripheral.Disconnected += disconnectedHandler;

        byte imageType = 0x00;
        if (softdevice != null) imageType |= 0x01;
        if (bootloader != null) imageType |= 0x02;
        if (application != null) imageType |= 0x04;

        await WriteData(dfuCharacteristicControlPoint, new byte[] { opCodeStartDfu, imageType }, true);

        byte[] imageSize = new byte[12];
        BufferAccess.WriteUInt32LittleEndian((uint)((softdevice != null) ? softdevice.Length : 0), imageSize, 0);
        BufferAccess.WriteUInt32LittleEndian((uint)((bootloader != null) ? bootloader.Length : 0), imageSize, 4);
        BufferAccess.WriteUInt32LittleEndian((uint)((application != null) ? application.Length : 0), imageSize, 8);
        await WriteData(dfuCharacteristicPacket, imageSize);

        // wait for response
        int index = await Task.Factory.StartNew(() => (WaitHandle.WaitAny(new WaitHandle[] { responseReceviedEvent, disconnectedEvent }, startDfuTimeout)),
          CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        if (index == WaitHandle.WaitTimeout)
        {
          Log.Warn ("Start DFU not acknowledged!");
          throw new DfuFailureException(DfuFailure.MissingAcknowledged);
        }
        else if (index == 1)
        {
          Log.Info("Disconnected... Maybe device enters bootloader.");
          throw new DfuFailureException(DfuFailure.MissingAcknowledged);
        }

        // check response
        if (responseValue != responseValueSuccess)
        {
          Log.Warn("Invalid response");
          throw new DfuFailureException(DfuFailure.InvalidResponse);
        }
      }
      finally
      {
        dfuCharacteristicControlPoint.NotificationReceived -= notificationReceviedHandler;
        peripheral.Disconnected -= disconnectedHandler;
      }
    }




    /// <summary>
    /// Starts the bootloader.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicControlPoint">DFU characteristic control point.</param>
    /// <param name="softdevice">Softdevice.</param>
    /// <param name="bootloader">Bootloader.</param>
    /// <param name="application">Application.</param>
    async Task StartBootloader(IPeripheral peripheral, ICharacteristic dfuCharacteristicControlPoint, byte[] softdevice, byte[] bootloader, byte[] application)
    {
      AutoResetEvent disconnectedEvent = new AutoResetEvent(false);

      EventHandler<DisconnectedEventArgs> disconnectedHandler = delegate (object sender, DisconnectedEventArgs e)
      {
        disconnectedEvent.Set();
      };

      Log.Debug("Start DFU...");

      try
      {
        peripheral.Disconnected += disconnectedHandler;

        byte imageType = 0x00;
        if (softdevice != null) imageType |= 0x01;
        if (bootloader != null) imageType |= 0x02;
        if (application != null) imageType |= 0x04;

        await WriteData(dfuCharacteristicControlPoint, new byte[] { opCodeStartDfu, imageType }, true);
        
        // wait for response
        int index = await Task.Factory.StartNew(() => (WaitHandle.WaitAny(new WaitHandle[] { disconnectedEvent }, startDfuTimeout)),
          CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        if (index == WaitHandle.WaitTimeout)
        {
          Log.Warn("Unable to enter bootloader!");
          throw new DfuFailureException(DfuFailure.InvalidResponse);
        }
        else
        {
          Log.Info("Disconnected... Device enters bootloader.");
        }
      }
      finally
      {
        peripheral.Disconnected -= disconnectedHandler;
      }
    }


    /// <summary>
    /// Reads the DFU revision.
    /// </summary>
    /// <returns>The DFU revision.</returns>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicRevision">DFU characteristic revision.</param>
    async Task<int> ReadDfuRevision(IPeripheral peripheral, ICharacteristic dfuCharacteristicRevision)
    {
      byte[] revisionData = null;
      revisionData = await ReadData(dfuCharacteristicRevision, true, 0);
      if (revisionData.Length != 2)
      {
        throw new DfuFailureException(DfuFailure.InvalidResponse);
      }
      return (BufferAccess.ReadUInt16LittleEndian(revisionData, 0));
    }


    /// <summary>
    /// Initializes the DFU parameters.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicPacket">DFU characteristic packet.</param>
    /// <param name="dfuCharacteristicControlPoint">DFU characteristic control point.</param>
    /// <param name="softdevice">Softdevice.</param>
    /// <param name="bootloader">Bootloader.</param>
    /// <param name="application">Application.</param>
    /// <param name="dfuRevision">DFU Revision</param>
    async Task InitializeDfuParameters(IPeripheral peripheral, ICharacteristic dfuCharacteristicPacket, ICharacteristic dfuCharacteristicControlPoint, byte[] softdevice, byte[] bootloader, byte[] application, int dfuRevision)
    {
      AutoResetEvent responseReceviedEvent = new AutoResetEvent(false);
      byte           responseValue = 0;

      EventHandler<NotificationReceivedEventArgs> notificationReceviedHandler = delegate(object sender, NotificationReceivedEventArgs e)
        {
          byte[] data = e.Data;
          if (data == null) return;

          if (   (data.Length >= 3)
              && (data[0] == opCodeResponseCode)
              && (data[1] == opCodeInitializeDfuParameters))
          {
            responseValue = data[2];
            Log.InfoFormat("Initialize DFU parameters response ({0})", responseValue);
            responseReceviedEvent.Set();
          }
        };

      // calculate CRC over all firmware parts
      UInt16 crc = 0xFFFF;
      if (softdevice != null) crc = Crc16Compute(softdevice, 0, softdevice.Length, crc);
      if (bootloader != null) crc = Crc16Compute(bootloader, 0, bootloader.Length, crc);
      if (application != null) crc = Crc16Compute(application, 0, application.Length, crc);


      Log.Debug ("Initialize DFU Parameters...");

      try
      {
        dfuCharacteristicControlPoint.NotificationReceived += notificationReceviedHandler;

        if (dfuRevision == 0)
        {
          await WriteData(dfuCharacteristicControlPoint, new byte[] {opCodeInitializeDfuParameters}, true, 2);

          byte[] dfuParameter = new byte[4];
          BufferAccess.WriteUInt32LittleEndian(crc, dfuParameter, 0);
          await WriteData(dfuCharacteristicPacket, dfuParameter);
        }
        else if ((dfuRevision >= 0x0005) && (dfuRevision <= 0x0008))
        {
          await WriteData(dfuCharacteristicControlPoint, new byte[] { opCodeInitializeDfuParameters, 0x00 }, true, 2);
          
          byte[] dfuParameter = new byte[14];
          BufferAccess.WriteUInt16LittleEndian(0, dfuParameter, 0); // Device Type
          BufferAccess.WriteUInt16LittleEndian(0, dfuParameter, 2); // Device Revision
          BufferAccess.WriteUInt32LittleEndian(0, dfuParameter, 4); // Application
          BufferAccess.WriteUInt16LittleEndian(1, dfuParameter, 8); // Softdevice List Length
          BufferAccess.WriteUInt16LittleEndian(0xFFFE, dfuParameter, 10); // Softdevice Any
          BufferAccess.WriteUInt16LittleEndian(crc, dfuParameter, 12); // Softdevice Any
          await WriteData(dfuCharacteristicPacket, dfuParameter);
          await WriteData(dfuCharacteristicControlPoint, new byte[] { opCodeInitializeDfuParameters, 0x01 }, true, 2);
        }
        else
        {
          throw new DfuFailureException(DfuFailure.NotSupported);
        }

        // wait for response
        if (!await Task.Run(() => { return (responseReceviedEvent.WaitOne(initializeDfuParametersTimeout)); }))
        {
          Log.Warn ("Initialize DFU parameters not acknowledged!");
          throw new DfuFailureException(DfuFailure.MissingAcknowledged);
        }
      }
      finally
      {
        dfuCharacteristicControlPoint.NotificationReceived -= notificationReceviedHandler;
      }
    }

    /// <summary>
    /// Transmits the data.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicPacket">DFU characteristic packet.</param>
    /// <param name="dfuCharacteristicControlPoint">DFU characteristic control point.</param>
    /// <param name="softdevice">Softdevice.</param>
    /// <param name="bootloader">Bootloader.</param>
    /// <param name="application">Application.</param>
    /// <param name="options">DFU options.</param>
    async Task TransmitFirmware(IPeripheral peripheral, ICharacteristic dfuCharacteristicPacket, ICharacteristic dfuCharacteristicControlPoint, byte[] softdevice, byte[] bootloader, byte[] application, DfuOptions options)
    {
      AutoResetEvent packetReceiptNotificationEvent = new AutoResetEvent(false);

      AutoResetEvent responseReceviedEvent = new AutoResetEvent(false);
      byte           responseValue = 0;

      EventHandler<NotificationReceivedEventArgs> notificationReceviedHandler = delegate(object sender, NotificationReceivedEventArgs e)
        {
          byte[] data = e.Data;
          if (data == null) return;

          if (   (data.Length >= 3)
              && (data[0] == opCodeResponseCode)
              && (data[1] == opCodeReceiveFirmwareImage))
          {
            responseValue = data[2];
            Log.InfoFormat("Firmware receive response ({0})", responseValue);
            packetReceiptNotificationEvent.Set(); // the firmware receive response may also be the packet receipt for the last few packets.
            responseReceviedEvent.Set();
          }
          else if ((data.Length >= 1) && (data[0] == opCodePacketReceiptNotification))
          {
            Log.Info("Packet receipt notification");
            packetReceiptNotificationEvent.Set();
          }
          else
          {
            Log.InfoFormat("Unexpected notification ({0})", BinaryString.ToString(data));
          }
        };

      Log.Debug ("Transmit firmware...");

      try
      {
        dfuCharacteristicControlPoint.NotificationReceived += notificationReceviedHandler;

        // enable packet receive notification
        Log.Info("Enable packet receive notification");
        await WriteData(dfuCharacteristicControlPoint, new byte[] { opCodePacketReceiptNotificationRequest, (byte)(options.PacketReceiveNotificationCount % 256), (byte)(options.PacketReceiveNotificationCount / 256) }, true, 2);

        // send receive firmware image OpCode
        Log.Info("Send receive firmware image command");
        await WriteData(dfuCharacteristicControlPoint, new byte[] {opCodeReceiveFirmwareImage}, true, 2);

        // send firmware
        int packetIndex = 0;

        // transmit softdevice
        if (softdevice != null)
        {
          packetIndex = await TransmitFirmwareData(dfuCharacteristicPacket, softdevice, packetIndex, packetReceiptNotificationEvent, DfuStatus.TransmitSoftdevice, options);
        }

        // transmit bootloader
        if (bootloader != null)
        {
          packetIndex = await TransmitFirmwareData(dfuCharacteristicPacket, bootloader, packetIndex, packetReceiptNotificationEvent, DfuStatus.TransmitBootloader, options);
        }

        // transmit application
        if (application != null)
        {
          packetIndex = await TransmitFirmwareData(dfuCharacteristicPacket, application, packetIndex, packetReceiptNotificationEvent, DfuStatus.TransmitApplication, options);
        }

        // wait for response
        if (!await Task.Run(() => { return (responseReceviedEvent.WaitOne(receiveFirmwareResponseTimeout)); }))
        {
          Log.Warn ("Receive firmware response not received!");
          throw new DfuFailureException(DfuFailure.MissingAcknowledged);
        }
      }
      finally
      {
        dfuCharacteristicControlPoint.NotificationReceived -= notificationReceviedHandler;
      }
    }

    /// <summary>
    /// Transmit firmware data.
    /// </summary>
    /// <param name="dfuCharacteristicPacket">DFU characteristic packet.</param>
    /// <param name="firmware">Firmware data.</param>
    /// <param name="initPacketIndex">Initial packet index</param>
    /// <param name="packetReceiptNotificationEvent">Event for received packet notifications.</param>
    /// <param name="status">Current DFU status.</param>
    /// <param name="options">Options used for DFU operation.</param>
    /// <returns>Packet index for next firmware data packet (only used if more firmware packets must be sent).</returns>
    async Task<int> TransmitFirmwareData(ICharacteristic dfuCharacteristicPacket, byte[] firmware, int initPacketIndex, AutoResetEvent packetReceiptNotificationEvent, DfuStatus status, DfuOptions options)
    {
      int offset = 0;
      int progressEventCount = 0;
      int packetIndex = initPacketIndex;

      if (Progress != null) Progress(this, new DfuProgressEventArgs(status));

      while (offset < firmware.Length)
      {
        // build packet
        int packetLength = Math.Min(firmware.Length - offset, TransmitFirmwarePacketSize);
        byte[] dataPacket = new byte[packetLength];
        Array.Copy(firmware, offset, dataPacket, 0, packetLength);

        // send packet
        await WriteData(dfuCharacteristicPacket, dataPacket, false);

        // update offset
        offset += packetLength;

        // increment packets
        packetIndex++;

        // event limit reached?
        progressEventCount += packetLength;
        if ((progressEventCount >= TransmitFirmwareProgressEventInterval)
            || (offset >= firmware.Length))
        {
          if (Progress != null) Progress(this, new DfuProgressEventArgs(status, offset, firmware.Length));
          progressEventCount -= TransmitFirmwareProgressEventInterval;
        }

        // wait for receive notification?
        if ((packetIndex % options.PacketReceiveNotificationCount) == 0)
        {
          if (!await Task.Run(() => { return (packetReceiptNotificationEvent.WaitOne(packetReceiptNotificationTimeout)); }))
          {
            Log.Warn("Packet receipt notification not received!");
            throw new DfuFailureException(DfuFailure.MissingAcknowledged);
          }
          packetReceiptNotificationEvent.Reset();
        }
      }

      return (packetIndex);
    }


    /// <summary>
    /// Validates the firmware.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicPacket">DFU characteristic packet.</param>
    /// <param name="dfuCharacteristicControlPoint">DFU characteristic control point.</param>
    async Task ValidateFirmware(IPeripheral peripheral, ICharacteristic dfuCharacteristicPacket, ICharacteristic dfuCharacteristicControlPoint)
    {
      AutoResetEvent responseReceviedEvent = new AutoResetEvent(false);
      byte           responseValue = 0;

      EventHandler<NotificationReceivedEventArgs> notificationReceviedHandler = delegate(object sender, NotificationReceivedEventArgs e)
        {
          byte[] data = e.Data;
          if (data == null) return;

          if (   (data.Length >= 3)
            && (data[0] == opCodeResponseCode)
            && (data[1] == opCodeValidateFirmware))
          {
            responseValue = data[2];
            Log.InfoFormat("Validate firmware response ({0})", responseValue);
            responseReceviedEvent.Set();
          }
        };

      Log.Debug ("Validate firmware...");

      try
      {
        dfuCharacteristicControlPoint.NotificationReceived += notificationReceviedHandler;

        await WriteData(dfuCharacteristicControlPoint, new byte[] {opCodeValidateFirmware}, true, 2);

        // wait for response
        if (!await Task.Run(() => { return (responseReceviedEvent.WaitOne(validateFirmwareTimeout)); }))
        {
          Log.Warn ("Validate firmware not acknowledged!");
          throw new DfuFailureException(DfuFailure.MissingAcknowledged);
        }
      }
      finally
      {
        dfuCharacteristicControlPoint.NotificationReceived -= notificationReceviedHandler;
      }
    }


    /// <summary>
    /// Activates the and reset.
    /// </summary>
    /// <param name="peripheral">Peripheral.</param>
    /// <param name="dfuCharacteristicControlPoint">DFU characteristic control point.</param>
    async Task ActivateImageAndReset(IPeripheral peripheral, ICharacteristic dfuCharacteristicControlPoint)
    {
      AutoResetEvent disconnectedEvent = new AutoResetEvent(false);

      EventHandler<DisconnectedEventArgs> disconnectedHandler = delegate(object sender, DisconnectedEventArgs e)
        {
          Log.Info("Disconnected!");
          disconnectedEvent.Set();
        };

      Log.Debug ("Activate image and reset...");

      try
      {
        peripheral.Disconnected += disconnectedHandler;

        int retry = 0;
        do
        {
          retry++;

          try
          {
            await WriteData(dfuCharacteristicControlPoint, new byte[] { opCodeActivateImageAndReset });
          }
          catch (Exception)
          {
            Log.Debug("Write failed, but it may be because the device did a reset.");
          }

          if (await Task.Run(() => { return (disconnectedEvent.WaitOne(activateImageAndResetTimeout)); }))
          {
            return;
          }

          Log.Warn ("Disconnect as acknowledge missing!");
        } while (retry < 3);
      }
      finally
      {
        peripheral.Disconnected -= disconnectedHandler;
      }

      throw new DfuFailureException(DfuFailure.MissingAcknowledged);
    }


    /// <summary>
    /// Update the specified peripheral, softdevice, bootloader and application (TAP).
    /// </summary>
    /// <param name="dfuPeripheral">Peripheral device to update.</param>
    /// <param name="softdevice">Byte array representing the new softdevice to be installed with the update. '<c>null</c>0 if the softdevice shouldn't be updated.</param>
    /// <param name="bootloader">Byte array representing the new bootloader to be installed with the update. '<c>null</c>0 if the bootloader shouldn't be updated.</param>
    /// <param name="application">Byte array representing the new application to be installed with the update. '<c>null</c>0 if the application shouldn't be updated.</param>
    /// <param name="options">Options the be used for the update. By default <c>null</c> is used and a set of default options is used. See <see cref="DfuOptions"/> for more informations.</param>
    public async Task UpdateAsync(IPeripheral dfuPeripheral,
                                  byte[] softdevice,
                                  byte[] bootloader,
                                  byte[] application,
                                  DfuOptions options = null)
    {
      IPeripheral peripheral = dfuPeripheral;
      DfuFailure failure = DfuFailure.None;
      Exception updateException = null;

      // check parameter
      if ((softdevice != null) && (softdevice.Length == 0)) throw new ArgumentException("InvalidParameter", "softdevice");
      if ((bootloader != null) && (bootloader.Length == 0)) throw new ArgumentException("InvalidParameter", "bootloader");
      if ((application != null) && (application.Length == 0)) throw new ArgumentException("InvalidParameter", "application");

      // if no compatible services are given create list with default set
      var defaultServiceList = new List<DfuServiceDefinition>();
      defaultServiceList.Add(new DfuServiceDefinition());
      IEnumerable<DfuServiceDefinition>  compatibleServices = defaultServiceList;

      // if no options are given we use the standard options
      if (options == null)
      {
        options = new DfuOptions();
      }

      // softdevice files include the MBR section (0x0000 - 0x0FFF) which has to be ignored in the update
      if (softdevice != null)
      {
        if (softdevice.Count() < 0x1000) throw new ArgumentException("InvalidParameter", "softdevice");
        byte[] softdeviceWoMbr = new byte[softdevice.Count() - 0x1000];
        Array.Copy(softdevice, 0x1000, softdeviceWoMbr, 0, softdeviceWoMbr.Count());
        softdevice = softdeviceWoMbr;
      }

      // update sequence
      try
      {
        ICharacteristic dfuCharacteristicPacket;
        ICharacteristic dfuCharacteristicControlPoint;
        ICharacteristic dfuCharacteristicRevision;

        int dfuRevision = 0;

        int startDfuAttempt = 0;
        bool dfuStarted = false;
        do
        {
          startDfuAttempt++;
          if (startDfuAttempt <= 1)
          {
            Log.InfoFormat("Start DFU of device '{0}'", peripheral.Name);
          }
          else
          {
            Log.InfoFormat("Start DFU of device '{0}' - Attempt #{1}", peripheral.Name, startDfuAttempt);
          }

          // ----- CONNECT -----
          if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.ConnectingDevice));
          int connectAttempt = 0;
          bool isConnected = false;
          do
          {
            connectAttempt++;
            if (connectAttempt <= 1)
            {
              Log.InfoFormat("Connecting '{0}'", peripheral.Name);
            }
            else
            {
              Thread.Sleep(connectNextAttemptDelay);
              Log.InfoFormat("Connecting '{0}' - Attempt #{1}", peripheral.Name, connectAttempt);
            }

            try
            {
              await Connect(peripheral);
              isConnected = true;
            }
            catch (Exception ex)
            {
              Log.Info(string.Format("Connect '{0}' failed - Exception", peripheral.Name), ex);
              if (connectAttempt >= 3) throw;
              connectAttempt++;
            }
          } while (!isConnected);

          // ------ INITIAL CONNECT DELAY -----
          if ((startDfuAttempt <= 1) && (options.DelayAfterInitialConnect > 0))
          {
            if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.ConnectDelay));
            await Task.Delay(options.DelayAfterInitialConnect);
          }

#if ANDROID
          // ----- CLEAR SERVICE CACHE (Android) -----
          // reset service cache in Android is not exposed in the SDK and even not in Xamarin -> we use JNI to invoke the internally available method "refresh"
          if (!Arendi.BleLibrary.Utility.Tweak.ResetServiceCache(peripheral))
          {
            Log.Info("Failed to clear service cache!");
          }
#endif // ANDROID

          // ----- DISCOVER SERVICES -----
          if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.DiscoverServices));
          await DiscoverServices(peripheral);

          // ----- FIND COMPTATIBLE SERVICE -----
          dfuCharacteristicControlPoint = null;
          dfuCharacteristicPacket = null;
          dfuCharacteristicRevision = null;
          bool serviceFound = false;
          var updateServiceDefinitions = compatibleServices as IList<DfuServiceDefinition> ??
                                         compatibleServices.ToList();
          foreach (DfuServiceDefinition serviceDefinition in updateServiceDefinitions)
          {
            var dfuService = peripheral.FindService(serviceDefinition.ServiceUuid.ToLower());
            if (dfuService != null)
            {
              dfuCharacteristicControlPoint = dfuService.GetCharacteristic(serviceDefinition.ControlPointUuid.ToLower());
              dfuCharacteristicPacket = dfuService.GetCharacteristic(serviceDefinition.PacketUuid.ToLower());
              dfuCharacteristicRevision = dfuService.GetCharacteristic(serviceDefinition.RevisionUuid.ToLower());

              // found services?
              if ((dfuCharacteristicPacket != null) && (dfuCharacteristicControlPoint != null) && (dfuCharacteristicRevision != null))
              {
                serviceFound = true;
                break;
              }
            }
          }

          // no service found ?
          if (!serviceFound)
          {
            Log.Info("DFU service not found!");
            throw new DfuFailureException(DfuFailure.NoCompatibleServiceFound);
          }

          // ------ READ DFU REVISION ------
          if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.ReadDfuRevision));
          dfuRevision = await ReadDfuRevision(peripheral, dfuCharacteristicRevision);
          Log.InfoFormat("DFU service revision: {0}", dfuRevision);

          /*
           * Read the version number if available.
           * The version number consists of 2 bytes: major and minor. Therefore f.e. the version 5 (00-05) can be read as 0.5.
           *
           * Currently supported versions are:
           *  * no DFU Version characteristic - we may be either in the bootloader mode or in the app mode. The DFU Bootloader from SDK 6.1 did not have this characteristic,
           *                                    but it also supported the buttonless update. Usually, the application must have had some additional services (like Heart Rate, etc)
           *                                    so if the number of services greater is than 3 (Generic Access, Generic Attribute, DFU Service) we can also assume we are in
           *                                    the application mode and jump is required.
           *
           *  * version = 1 (major = 0, minor = 1) - Application with DFU buttonless update supported. A jump to DFU mode is required.
           *
           *  * version = 5 (major = 0, minor = 5) - Since version 5 the Extended Init Packet is required. Keep in mind that if we are in the app mode the DFU Version characteristic
           *  								  still returns version = 1, as it is independent from the DFU Bootloader. The version = 5 is reported only after successful jump to
           *  								  the DFU mode. In version = 5 the bond information is always lost. Released in SDK 7.0.0.
           *
           *  * version = 6 (major = 0, minor = 6) - The DFU Bootloader may be configured to keep the bond information after application update. Please, see the {@link #EXTRA_KEEP_BOND}
           *  								  documentation for more information about how to enable the feature (disabled by default). A change in the DFU bootloader source and
           *  								  setting the {@link DfuServiceInitiator#setKeepBond} to true is required. Released in SDK 8.0.0.
           *
           *  * version = 7 (major = 0, minor = 7) - The SHA-256 firmware hash is used in the Extended Init Packet instead of CRC-16. This feature is transparent for the DFU Service.
           *
           *  * version = 8 (major = 0, minor = 8) - The Extended Init Packet is signed using the private key. The bootloader, using the public key, is able to verify the content.
           *  								  Released in SDK 9.0.0 as experimental feature.
           *  								  Caution! The firmware type (Application, Bootloader, SoftDevice or SoftDevice+Bootloader) is not encrypted as it is not a part of the
           *  								  Extended Init Packet. A change in the protocol will be required to fix this issue.
           */

          // check DFU
          if (dfuRevision <= 1)
          {
            // we are in appliaction
             
            // ------ ENABLE NOTIFICATION -----
            if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.PrepareService));
            await UpdateNotification(dfuCharacteristicControlPoint, true);

            // add delay before Start DFU (see pull request: https://github.com/NordicSemiconductor/Android-DFU-Library/pull/11)
            if (options.DelayBeforeStartDfu > 0)
            {
              Thread.Sleep(options.DelayBeforeStartDfu);
            }

            // ------ START UPDATE -----
            if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.StartUpdate));
            try
            {
              await StartBootloader(peripheral, dfuCharacteristicControlPoint, softdevice, bootloader, application);
            }
            catch (Exception)
            {
              Log.Info("Failed to start DFU!");
              if (startDfuAttempt >= 3) throw;
            }
          }
          else if (dfuRevision < 5)
          {
            // DFU mode 2 - 4 not supported
            Log.Error("DFU revision not supported!");
            throw new Exception("DFU revision not supported!");
          }
          else
          {
            // ------ ENABLE NOTIFICATION -----
            if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.PrepareService));
            await UpdateNotification(dfuCharacteristicControlPoint, true);

            // add delay before Start DFU (see pull request: https://github.com/NordicSemiconductor/Android-DFU-Library/pull/11)
            if (options.DelayBeforeStartDfu > 0)
            {
              Thread.Sleep(options.DelayBeforeStartDfu);
            }

            // ------ START UPDATE -----
            if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.StartUpdate));
            try
            {
              await
                StartDFU(peripheral, dfuCharacteristicPacket, dfuCharacteristicControlPoint, softdevice, bootloader,
                  application);
              dfuStarted = true;
            }
            catch (Exception)
            {
              Log.Info("Failed to start DFU!");
              if (startDfuAttempt >= 3) throw;
            }

            // disconnect if start DFU failed (the device may restart into bootloader here)
            if (!dfuStarted)
            {
              if (peripheral.State != State.Disconnected)
              {
                await Disconnect(peripheral);
              }

              Thread.Sleep(startDfuNextAttemptDelay);
            }
          }
        } while (!dfuStarted);


        // ------ INITIALIZE UPDATE PARAMETERS -----
        if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.InitializeUpdateParameters));
        await
          InitializeDfuParameters(peripheral, dfuCharacteristicPacket, dfuCharacteristicControlPoint, softdevice,
            bootloader, application, dfuRevision);

        // ------ TRANSMIT FIRMWARE -----
        await TransmitFirmware(peripheral, dfuCharacteristicPacket, dfuCharacteristicControlPoint, softdevice, bootloader, application, options);

        // ------ VALIDATE FIRMWARE -----
        if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.ValidateFirmware));
        await ValidateFirmware(peripheral, dfuCharacteristicPacket, dfuCharacteristicControlPoint);

        // ------ ACTIVATE IMAGE & RESET -----
        if (Progress != null) Progress(this, new DfuProgressEventArgs(DfuStatus.ActivateImageAndReset));
        await ActivateImageAndReset(peripheral, dfuCharacteristicControlPoint);

        Log.InfoFormat("DFU update of device '{0}' completed", peripheral.Name);
      }
      catch (DfuFailureException ex)
      {
        updateException = ex;
        failure = ex.Failure;
      }
      catch (BleException ex)
      {
        updateException = ex;
        switch (ex.Result)
        {
          case BleResult.Success:
            break;
          case BleResult.Timeout:
            failure = DfuFailure.UnableToConnect;
            break;
          case BleResult.NotConnected:
            failure = DfuFailure.UnableToConnect;
            break;
          case BleResult.NotConnectable:
            failure = DfuFailure.UnableToConnect;
            break;
          case BleResult.UnableToConnect:
            failure = DfuFailure.UnableToConnect;
            break;
          case BleResult.NotSupported:
            failure = DfuFailure.NotSupported;
            break;
          default:
            failure = DfuFailure.UnknownError;
            break;
        }
      }
      catch (Exception ex)
      {
        updateException = ex;
        failure = DfuFailure.UnknownError;
      }

      // Log
      if (failure == DfuFailure.None)
      {
        Log.Info("DFU update completed with success");
      }
      else
      {
        Log.Error("DFU update failed", updateException);
      }

      // call completed event
      if (Completed != null) Completed(this, new DfuCompletedEventArgs(failure));

      // disconnect
      try
      {
        await Disconnect(peripheral);
      }
      catch (Exception)
      {
      }

      // throw saved exception
      if (updateException != null) throw updateException;
    }


    private async Task UpdateThread(object[] parameter)
    {
      await UpdateAsync((IPeripheral) parameter[0],
        (byte[]) parameter[1],
        (byte[]) parameter[2],
        (byte[]) parameter[3],
        (DfuOptions)parameter[4]);
    }


    /// <summary>
    /// Update a peripheral firmware asynchronously (EAP).
    /// </summary>
    /// <param name="peripheral">Peripheral device to update.</param>
    /// <param name="softdevice">Byte array representing the new softdevice to be installed with the update. '<c>null</c>0 if the softdevice shouldn't be updated.</param>
    /// <param name="bootloader">Byte array representing the new bootloader to be installed with the update. '<c>null</c>0 if the bootloader shouldn't be updated.</param>
    /// <param name="application">Byte array representing the new application to be installed with the update. '<c>null</c>0 if the application shouldn't be updated.</param>
    /// <param name="options">Options the be used for the update. By default <c>null</c> is used and a set of default options is used. See <see cref="DfuOptions"/> for more informations.</param>
    public void Update(IPeripheral peripheral,
                       byte[] softdevice,
                       byte[] bootloader,
                       byte[] application,
                       DfuOptions options = null)
    {
      if (updateThread != null) throw new Exception("Already a update pending");

      updateThread = new Thread(async delegate()
      {
        try
        {
          await UpdateThread(new object[]
          {
            peripheral,
            WordSizeBinary(softdevice),
            WordSizeBinary(bootloader),
            WordSizeBinary(application),
            options
          });
        }
        catch (Exception ex)
        {
          Log.Error("Update failed", ex);
        }
        finally
        {
          // remove thread
          updateThread = null;
        }
      });
      updateThread.Start();
    }


    private static byte[] WordSizeBinary(byte[] image)
    {
      // no image given?
      if (image == null)
        return(null);

      // image already word sized?
      if ((image.Length % 4) == 0)
        return image;

      // copy image to a word sized binary
      byte[] newImage = new byte[((image.Length / 4) + 1) * 4];
      Array.Copy(image, newImage, image.Length);
      return (newImage);
    }


    static UInt16 Crc16Compute(byte[] data, int offset, int size, UInt16 crc)
    {
      int i;

      for (i = 0; i < size; i++)
      {
        crc  = (UInt16)((byte)(crc >> 8) | (crc << 8));
        crc ^= data[offset + i];
        crc ^= (UInt16)((byte)(crc & 0xff) >> 4);
        crc ^= (UInt16)((crc << 8) << 4);
        crc ^= (UInt16)(((crc & 0xff) << 4) << 1);
      }

      return crc;
    }

    #endregion

  }
}

