namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Optins for a DFU operation.
  /// </summary>
  public class DfuOptions
  {
    #region Constants
    /// <summary>
    /// Default number of packets that are confimed by the bootloader.
    /// </summary>
    public const int PacketReceiveNotificationCountDefault = 5;

    /// <summary>
    /// Default delay after initial connect [ms].
    /// </summary>
    public const int DelayAfterInitialConnectDefault = 0;

    /// <summary>
    /// Default before start of the DFU [ms].
    /// </summary>
    public const int DelayBeforeStartDfuDefault = 1000;
    #endregion

    #region Properties
    /// <summary>
    /// Number of data packets before a notification is expected.<br/>
    /// Default: <see cref="PacketReceiveNotificationCountDefault"/>
    /// </summary>
    public int PacketReceiveNotificationCount { get; set; }


    /// <summary>
    /// Delay after initial connect [ms].<br/>
    /// This delay may be added to ensure that the peripheral had enought time to
    /// change the connection parameters for it's need. Especialy on Android devices
    /// it may make sence because Android per default connects with 20s connection
    /// timeout. On a reset into bootloader the Android will not reconnect until the
    /// connection timeout has elapsed.
    /// <br/>
    /// Default: <see cref="DelayAfterInitialConnectDefault"/>
    /// </summary>
    public int DelayAfterInitialConnect { get; set; }



    /// <summary>
    /// Delay added before starting the DFU [ms].<br/>
    /// see https://github.com/NordicSemiconductor/Android-DFU-Library/pull/11
    /// <br/>
    /// Default: <see cref="DelayBeforeStartDfuDefault"/>
    /// </summary>
    public int DelayBeforeStartDfu { get; set; }

    #endregion

    #region Methods
    /// <summary>
    /// Create default DFU options. They can be modified after creation.
    /// </summary>
    public DfuOptions()
    {
      PacketReceiveNotificationCount = PacketReceiveNotificationCountDefault;
      DelayAfterInitialConnect = DelayAfterInitialConnectDefault;
      DelayBeforeStartDfu = DelayBeforeStartDfuDefault;
    }
    #endregion
  }
}
