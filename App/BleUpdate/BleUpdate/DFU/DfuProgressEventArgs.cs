using System;

namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Event arguments for the DFU progress.
  /// </summary>
  public class DfuProgressEventArgs : EventArgs
  {
    /// <summary>
    /// Get the status of the DFU process.
    /// </summary>
    public DfuStatus Status
    {
      get;
      private set;
    }

    /// <summary>
    /// Get the total number to be reached in this DFU state. If the state has no sub steps this value is 0.
    /// </summary>
    public int Total {
      get;
      private set;
    }

    /// <summary>
    /// Get the index number in this DFU state. If the state has no sub steps this value is 0.
    /// </summary>
    public int Index {
      get;
      private set;
    }

    /// <summary>
    /// Create DFU progress event arguments.
    /// </summary>
    /// <param name="status">Status of the DFU progress.</param>
    public DfuProgressEventArgs(DfuStatus status)
    {
      Status = status;
      Index = 0;
      Total = 0;
    }


    /// <summary>
    /// Create DFU progress event arguments.
    /// </summary>
    /// <param name="status">Status of the DFU progress.</param>
    /// <param name="index">Index of the sub steps.</param>
    /// <param name="total">Total number of sub steps.</param>
    public DfuProgressEventArgs(DfuStatus status, int index, int total)
    {
      Status = status;
      Index = index;
      Total = total;
    }
  }
}

