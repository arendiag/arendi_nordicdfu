using System;

namespace Arendi.BleLibrary.Service.NordicDFU
{
  /// <summary>
  /// Exception thrown when a asynchronously started update failed (TAP). 
  /// </summary>
  public class DfuFailureException : Exception
  {
    /// <summary>
    /// Get the failure that cased the update to fail.
    /// </summary>
    public DfuFailure Failure { get; private set; }

    /// <summary>
    /// Create the DFU failure exception.
    /// </summary>
    /// <param name="failure">Failure that cased the update to fail.</param>
    /// <param name="innerException">Inner exception.</param>
    public DfuFailureException(DfuFailure failure, Exception innerException) : base(string.Format("DFU update failed with failure '{0}'", failure), innerException)
    {
      Failure = failure;
    }

    /// <summary>
    /// Create the DFU failure exception.
    /// </summary>
    /// <param name="failure">Failure that cased the update to fail.</param>
    public DfuFailureException(DfuFailure failure)
      : base(string.Format("DFU update failed with failure '{0}'", failure))
    {
      Failure = failure;
    }
  }
}
