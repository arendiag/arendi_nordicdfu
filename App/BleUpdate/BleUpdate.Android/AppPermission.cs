﻿using System;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;

namespace BleUpdate.Droid
{
  public class AppPermission
  {
    private static int PERMISSION_REQUEST_COARSE_LOCATION = 947834;
    private static  string LocationPopupServiceTitle = "This app needs location access";
    private static string LocationPopupServiceText = "Please enable location service so this app can detect BLE devices.";
    private static string LocationPopupRuntimePermissionTitle = "This app needs location runtime permission";
    private static string LocationPopupRuntimePermissionText = "Please grant location access so this app can detect BLE devices.";

    public static void RequestLocation_Android6(Activity context)
    {
      if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
      {

        var locationMode = Settings.Secure.GetInt(context.ContentResolver, Settings.Secure.LocationMode);

        if (locationMode == 0)
        {
          var dialog = CreateDialog(context, LocationPopupServiceTitle, LocationPopupServiceText);
          dialog.SetPositiveButton("Ok", delegate
          {
          }).SetOnDismissListener(new OnDismissListener(() =>
          {
            var myIntent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
            context.StartActivity(myIntent);
          }));
          dialog.Show();
          return;
        }


        if (context.CheckSelfPermission(Manifest.Permission.AccessCoarseLocation) != Permission.Granted)
        {
          var dialog = CreateDialog(context, LocationPopupRuntimePermissionTitle, LocationPopupRuntimePermissionText);
          dialog.SetPositiveButton("Ok", delegate { }).SetOnDismissListener(new OnDismissListener(() =>
          {
            context.RequestPermissions(new String[] { Manifest.Permission.AccessCoarseLocation }, PERMISSION_REQUEST_COARSE_LOCATION);
          }));
          dialog.Show();
        }
      }
    }


    private sealed class OnDismissListener : Java.Lang.Object, IDialogInterfaceOnDismissListener
    {
      private readonly Action action;

      public OnDismissListener(Action action)
      {
        this.action = action;
      }

      public void OnDismiss(IDialogInterface dialog)
      {
        this.action();
      }
    }

    private static AlertDialog.Builder CreateDialog(Context context, string title, string message)
    {
      var builder = new AlertDialog.Builder(context);
      builder.SetTitle(title);
      builder.SetMessage(message);

      return builder;
    }
  }
}