﻿using Android.Content;

using BleUpdate;
using BleUpdate.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(ApplicationEnvironment))]
namespace BleUpdate.Droid
{
  public class ApplicationEnvironment : IApplicationEnvironment
  {
    public string GetVersion()
    {
      Context context = Android.App.Application.Context;
      //return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName + " - " + context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionCode;
      return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
    }
  }
}