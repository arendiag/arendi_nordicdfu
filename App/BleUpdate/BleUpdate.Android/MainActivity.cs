﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Content;

using Arendi.DotNETLibrary.Log;

namespace BleUpdate.Droid
{

  [Activity(Icon = "@drawable/ic_launcher", Label = "Nordic DFU Demo", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]

  public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
  {
    private static readonly ILog Log = Arendi.DotNETLibrary.Log.LogManager.GetLogger("BleUpdate");

    protected override void OnCreate(Bundle bundle)
    {
      Log.Level = LogLevel.All;
      base.OnCreate(bundle);
      App.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density); // device independent pixels
      App.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density); // device independent pixels

      Context context = Android.App.Application.Context;
      var packageInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
      App.Version = $"{packageInfo.VersionName} ({packageInfo.VersionCode})";
      global::Xamarin.Forms.Forms.Init(this, bundle);

      LoadApplication(new App()); // method is new in 1.3
    }

    protected override void OnResume()
    {
      AppPermission.RequestLocation_Android6(this);
      base.OnResume();
    }

  }
}

