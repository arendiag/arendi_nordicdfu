using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Views;
using Android.OS;

namespace BleUpdate.Droid
{
  [Activity(Label = "BLE Update", Icon = "@drawable/ic_launcher", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, Theme = "@style/Theme.Splash", NoHistory = true)]
  public class SplashscreenActivity : Activity
  {
    protected override void OnCreate(Bundle bundle)
    {
      Window.DecorView.SystemUiVisibility = (StatusBarVisibility)
        (SystemUiFlags.LayoutStable
      | SystemUiFlags.LayoutHideNavigation
      | SystemUiFlags.LayoutFullscreen
      | SystemUiFlags.HideNavigation// hide nav bar
      | SystemUiFlags.Fullscreen// hide status bar
      | SystemUiFlags.ImmersiveSticky);

      base.OnCreate(bundle);

      var intent = new Intent(this, typeof(MainActivity));
      StartActivity(intent);
    }
  }
}

