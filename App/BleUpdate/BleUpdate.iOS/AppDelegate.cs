using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using Xamarin.Forms;

namespace BleUpdate
{
  [Register("AppDelegate")]
  public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
  {
    // class-level declarations
    //UIWindow window;

    public override bool FinishedLaunching (UIApplication app, NSDictionary options)
    {
      global::Xamarin.Forms.Forms.Init ();

      App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;
      App.ScreenHeight = (int)UIScreen.MainScreen.Bounds.Height;
      var shortVersionString = NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString").ToString();
      var versionString = NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion").ToString();
      App.Version = String.Format("{0} ({1})", shortVersionString, versionString);

      LoadApplication (new App ());  // method is new in 1.3

      return base.FinishedLaunching (app, options);
    }

    public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
    {
      // NOTE: Don't call the base implementation on a Model class
      // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
      return (true);
    }
  }
}
