﻿using BleUpdate;

using Foundation;

[assembly: Xamarin.Forms.Dependency(typeof(ApplicationEnvironment))]
namespace BleUpdate
{
  public class ApplicationEnvironment : IApplicationEnvironment
  {
    public string GetVersion()
    {
      return NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"].ToString() + " - " + NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();
    }
  }
}