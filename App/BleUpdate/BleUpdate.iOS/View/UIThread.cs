﻿using System;

using Xamarin.Forms;

namespace BleUpdate.View
{
  public static class UIThread
  {
    public static void InvokedOnMainThread(Action action)
    {
      Device.BeginInvokeOnMainThread(() =>
        {
          action();
        });
    }
  }
}

