# License: Arendi BLE Library for XAMARIN 
##START OF LICENSE AGREEMENT 
###ARENDI SOFTWARE LICENSE AGREEMENT (4 th  of December 2015) 
By accepting the terms of this document you enter into an agreement with Arendi AG, whose 
principal place of business is in Hombrechtikon, Switzerland; and therefore, for good and 
valuable consideration, the receipt and sufficiency of which is hereby acknowledged, Arendi 
and you agree as follows:  

LICENSE : Arendi BLE Library 

In exchange for the applicable initial fee and for the term described below, Arendi hereby 
grants to you, with respect to all copies of Arendi BLE Library in object code form, a 
nonexclusive and worldwide license to: 

(a) to use the Arendi Library in connection to one single client application (Business License) 
or companywide (Enterprise License) including its subsidiaries owned by you, referred to 
Arendi BLE Licence Model, release June 07, 2016. 
You may redistribute the ARENDI BLE Library with the end user application to an unlimited 
number of computers or mobile devices. The licensing mechanism is based on the Company 
and Product assembly attributes of the entry assembly that hosts the ARENDI BLE Library. 

(b) combine the ARENDI BLE Library with other software to form a combined product with 
added primary functionality in such a way that the functionality of the ARENDI BLE Library 
is secondary to the combined product, provided any portion of the ARENDI BLE Library so 
combined continues to be subject to the terms and conditions of this agreement. 

(c) brand, distribute, sublicense and support the Combined Product to third parties as your 
product, provided that the Combined Product is an end-product and not a software 
development toolkit or a software development environment and does not directly compete 
with the ARENDI BLE Library or any other product of ARENDI, and the Combined Product 
does not in any way expose the Application Product Interface of the ARENDI BLE Library, 
in which cases branding, distribution, and/or sublicensing is prohibited.  

ACCEPTANCE. You acknowledge and confirm that you have been granted enough time to 
investigate and test whether the ARENDI BLE Library performs in accordance to your 
requirements. 

SUPPORT. As an integral part of your license, ARENDI will, for the duration of your license, 
provide support services to you, which include development, support, maintenance and 
distribution of updates and upgrades. Support services will only be provided in connection to 
the most recent version of the ARENDI BLE Library. 

PERSONAL KEY. You understand and agree that ARENDI will provide you a personal 
private key to activate your library license. This key must be treated and stored private and 
safe and used in a secure way in the end product, not enabling third parties to re-engineer the 
key.  

TERM AND TERMINATION LICENSE. Unless stated otherwise in writing by ARENDI, 
licenses hereunder, including support services, will automatically terminate in case the annual 
fee has not been fully paid upon thirty (30) days of the anniversary of the license. In such case 
you may no longer combine, brand, distribute, sublicense or use the Arendi BLE Library in 
any other way. Furthermore, ARENDI may revoke and terminate your license if you 
materially breach any provision of this agreement.  

OWNERSHIP. You agree that as between you and ARENDI, ARENDI is the sole and 
exclusive owner of all right, title and interest worldwide, including in countries where such 
rights have not yet been used, exercised or registered, in the ARENDI BLE Library and you 
will not directly or indirectly question, attack, contest or in any other manner impugn the 
value, validity or enforceability of ARENDI BLE Library or any other software from 
ARENDI. You will not remove, alter, cover or obfuscate any patent or copyright notice or 
other proprietary or restrictive notice or legend placed or embedded by ARENDI on the 
ARENDI BLE Library. 

REVERSE ENGINEERING. You will refrain from any reverse engineering, disassembly, or 
any other attempt to obtain ARENDI BLE Library in source code or assembly code form. 

PAYMENT. You warrant and guarantee that the initial fee, will be paid in full immediately 
and irrevocably upon receipt of ARENDIs invoice by email or any other way of 
communication.  

INDEMNIFICATION. You agree to indemnify ARENDI and to undertake to defend and hold 
ARENDI harmless from and against any and all claims, demands, causes of action, damages, 
liabilities, costs and expenses, including reasonable attorney fees, arising from your activities 
under this agreement, including, but not limited to, any product liability claims or actions, or 
the unauthorized use of any trademark, copyright, patent, process or method. 

DISCLAIMERS. ARENDI specifically disclaims all representations and warranties, including 
implied warranties of merchantability, fitness for a particular purpose and non-infringement 
(whether express, implied, or statutory) regarding, relating to, connected with or arising out of 
ARENDI BLE Library. 

LIMITATIONS ON KINDS OF DAMAGES. In no event will either party be liable to the 
other for any punitive, exemplary, indirect, special, incidental or consequential damages of 
any kind (including loss of profits, loss of use, business interruption, loss of data or costs of 
procurement of substitute goods, technologies or services or cost of cover) in connection with 
or arising out of this agreement, whether alleged as a breach of contract or tortious conduct, 
including negligence, even if such party has been advised of the possibility of such damages. 

LIMITATION ON AMOUNT OF DAMAGES. Each party liability to the other under this 
agreement or the termination of this agreement for damages of any kind, including restitution, 
will not, in any event, exceed, in the aggregate, the fees and payments paid by you. 

ASSIGNMENT. You may not assign, sublicense or otherwise transfer any rights under this 
agreement.  

GOVERNING LAW AND JURISDICTION. This agreement shall be governed by and 
construed in accordance with the laws of Switzerland. With respect to any disputes arising out 
of this agreement, the parties hereby submit exclusively to the personal jurisdiction of the courts in Zurich, Switzerland. The parties consent and agree that each such court is a 
convenient forum for, and has proper venue over, the resolution of all legal actions, 
proceedings and disputes arising out of or relating to this agreement. Notwithstanding the 
foregoing, ARENDI may elect to enforce any of its rights hereunder in the local courts of 
your country.

##END OF LICENSE AGREEMENT 
If you have any questions please contact us [info@arendi.ch](mailto:info@arendi.ch?subject=License%20question%20to%20Arendi%20BLE%20Library).
