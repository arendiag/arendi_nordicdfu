Arendi Ble Library™
===========================================

The Arendi BLE Library™ offers an easy and simple start in the world of Bluetooth Low Energy. By using the library your engineers don’t have to care about the differences of the various target platforms. They can rely on a unified and well documented interface and benefit from predefined automatisms.

Follow these steps to be able to use this library:

* download and install the component from Xamarin component store
* use our demo app as a quick start
* use the evaluation edition of this library with a 2 minutes run limit or purchase a licence key to enable the full version from [www.arendi.ch](http://ble-library.arendi.ch/)


Using the Arendi BleLibrary™
---------------------------------------

Create two derived classes, one from `EnhancedPeripheral` and one from `PeripheralManager<TPeripheral>`.  

An `EnhancedPeripheral` represents a BLE peripheral device.
Use this class to establishe a connection to a real device and read or write characteristics and register for notifications.

The `PeripheralManager` is used to manage the peripherals. Use this class to scan for peripherals and retieve a list of visible peripherals. 

The following code snipped should give you a rough overview of the simple API: 

```csharp
// instantiate a Central device
var central = CentralFactory.GetCentral();
var peripheralManager = new ReferencePeripheralManager(central);
// start the scan for peripherals
peripheralManager.StartScan();
...
// wait until the first peripheral is detected
...
var peripheral = peripheralManager.Peripherals.First();
// you can use traditional or TAP API
await peripheral.EstablishAsync();
// read, write characteristics or register for notifications
await peripheral.WriteYourPropertyAsync("hello")

```

A minimal implementation can be found in our sample projects.
 
Keep in mind to purchase a licence if you are using the Arendi Ble Library™ in a project.
Without a valid licence key the Library will stop working after 2 minutes.


Purchase a licence key
--------------------------

Visit [www.arendi.ch](http://ble-library.arendi.ch) and fill out the contact form.


Documentation
----------------
Click here [here](http://www.arendi.ch/files/BLELibrary/Documentation/) to visit the full online API documentation.


Support
-------

Questions? Contact [info@arendi.ch](mailto:info@arendi.ch?subject=Question%20about%20Arendi%20BLE%20Library).
