﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Arendi.BleLibrary.Local;
using ReferenceApp.Model;
using Xamarin.Forms;

namespace ReferenceApp.View
{
  /// <summary>
  /// A class to simplify the navigation between the tabs. All tabs are created by this class.
  /// </summary>
  class AppNavigation : TabbedPage
  {
    private readonly MainPage mainPage;
    private TabContentPage currentPage;

    public AppNavigation()
    {
      this.Title = "Arendi BleLibrary Reference App";
      var selectDevicePage = new SelectDevicePage(this);
      mainPage = new MainPage();
      var infoPage = new InfoPage();
      this.Children.Add(selectDevicePage);
      this.Children.Add(mainPage);
      this.Children.Add(infoPage);
      this.CurrentPage = selectDevicePage;
      this.PropertyChanged += OnPropertyChanged;
      currentPage = selectDevicePage;
      currentPage.PageSelected();
    }

    /// <summary>
    /// Delegate to detect when the page was changed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName.Equals("CurrentPage"))
      {
        currentPage.PageDeselected();
        var selected = this.CurrentPage as TabContentPage;
        if (selected != null)
        {
          currentPage = selected;
          currentPage.PageSelected();
        }
      }
    }

    /// <summary>
    /// Call this method to navigate to the main page.
    /// </summary>
    internal void NavigateToMainPage()
    {
      if (mainPage == null)
      {
        return;
      }
      this.CurrentPage = mainPage;
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();
      // Check if bluetooth is enabled and notify the user if it is disabled
      if (!App.BluetoothEnabled)
      {
        DisplayAlert("Bluetooth", App.EnableBluetoothMessage, "OK");
      }
    }
  }
}
