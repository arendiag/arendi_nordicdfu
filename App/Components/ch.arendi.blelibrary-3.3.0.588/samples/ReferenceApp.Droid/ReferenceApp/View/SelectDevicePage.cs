using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using ReferenceApp.Model;

namespace ReferenceApp.View
{
  /// <summary>
  /// A page to select an advertising peripheral.
  /// </summary>
  public class SelectDevicePage : TabContentPage
  {
    /// <summary>
    /// A helper class to display an item on the list view
    /// </summary>
    private class DeviceItem
    {
      public ReferencePeripheral Peripheral {
        get;
        private set;
      }

      public string Name {
        get {
          return (ToString ());
        }
      }

      public DeviceItem(ReferencePeripheral peripheral)
      {
        Peripheral = peripheral;
      }

      public override string ToString()
      {
        if (string.IsNullOrEmpty (Peripheral.Name)) {
          return Peripheral.Uuid.ToString();
        }
        return Peripheral.Name;
      }
    }

    private readonly ObservableCollection<DeviceItem> deviceListSource;
    private readonly AppNavigation navigation;

    internal SelectDevicePage(AppNavigation navigation)
    {
      this.navigation = navigation;
      Title = "Device";
      Icon = "ic_list.png";
      NavigationPage.SetBackButtonTitle(this, "");

      deviceListSource = new ObservableCollection<DeviceItem>();
      var deviceList = new ListView();
      deviceList.ItemsSource = deviceListSource;
      deviceList.ItemSelected += OnItemSelected;
      deviceList.ItemTemplate = new DataTemplate(typeof(TextCell));
      deviceList.ItemTemplate.SetBinding(TextCell.TextProperty, "Name");

      Content = new StackLayout
      {
        VerticalOptions = LayoutOptions.FillAndExpand,
        Children = { deviceList }
      };
    }

    /// <summary>
    /// Sets the PeripheralManager.Selected to the selected item and switches to the main page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
    {
      App.PeripheralManager.Selected = ((DeviceItem)e.SelectedItem).Peripheral;
      navigation.NavigateToMainPage();
    }

    #region Methods
    /// <summary>
    /// Attach the event handlers on appearing
    /// </summary>
    protected override void OnAppearing()
    {
      base.OnAppearing ();
      App.PeripheralManager.PeripheralAdded += OnPeripheralAdded;
      App.PeripheralManager.PeripheralRemoved += OnPeripheralRemoved;
      if (App.ExceptionMessage != null)
      {
        DisplayAlert("Evaluation Version", App.ExceptionMessage, "Ok");
      }
      UpdatePeripheralList();
    }

    /// <summary>
    /// Starts the scan when this page is selected.
    /// </summary>
    internal override void PageSelected()
    {
      try
      {
        App.PeripheralManager.StartScan();
      }
      catch (Exception ex)
      {
        App.ExceptionMessage = ex.Message;
      }
    }

    /// <summary>
    /// Stops the scan when this page is left. 
    /// </summary>
    internal override void PageDeselected()
    {
      App.PeripheralManager.StopScan();
    }

    /// <summary>
    /// Detach the event handlers on disappearing
    /// </summary>
    protected override void OnDisappearing()
    {
      App.PeripheralManager.PeripheralAdded -= OnPeripheralAdded;
      App.PeripheralManager.PeripheralRemoved -= OnPeripheralRemoved;
      base.OnDisappearing ();
    }

    /// <summary>
    /// Update the UI peripheral list when a peripheral was found by the peripheral manager
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void OnPeripheralAdded (object sender, EventArgs e)
    {
      UpdatePeripheralList();
    }

    /// <summary>
    /// Update the UI peripheral list when a peripheral was invisible for some time
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void OnPeripheralRemoved(object sender, EventArgs e)
    {
      UpdatePeripheralList();      
    }

    /// <summary>
    /// Update UI list with peripherals to the peripheral manager based list
    /// </summary>
    private void UpdatePeripheralList()
    {
      Device.BeginInvokeOnMainThread(
        () =>
        {
          // Get a list of all peripherals in the peripheral manager
          IEnumerable<ReferencePeripheral> realPeripheralEnumerable = App.PeripheralManager.Peripherals;
          var peripheralListReal = realPeripheralEnumerable as IList<ReferencePeripheral> ?? realPeripheralEnumerable.ToList();

          // select items to remove from the UI list if they are not in the list of the peripheral manager
          List<DeviceItem> itemsToRemove = new List<DeviceItem>();
          foreach (var item in deviceListSource)
          {
            if (!peripheralListReal.Contains(item.Peripheral))
            {
              itemsToRemove.Add(item);
            }
          }

          // remove items to remove
          foreach (var item in itemsToRemove)
          {
            deviceListSource.Remove(item);
          }

          // add new items to the UI list if they exist in the peripheral manager list and not in the UI list
          foreach (var item in peripheralListReal)
          {
            // check if peripheral already in the list
            if (!deviceListSource.Any(model => model.Peripheral == item))
            {
              deviceListSource.Add(new DeviceItem(item));
            }
          }
        }
      );

    }
    #endregion
  }
}

