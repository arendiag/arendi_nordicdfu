﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ReferenceApp.View
{
  /// <summary>
  /// On Windows Phone, it's not possible to detect a change of the tab in focus. This class is used to notify 
  /// the page if it is selected or not
  /// </summary>
  public class TabContentPage : ContentPage
  {
    /// <summary>
    /// Set the padding for all TabContentPages.
    /// </summary>
    public TabContentPage()
    {
      Padding = new Thickness(Device.OnPlatform(20, 10, 10), Device.OnPlatform(30, 20, 10), Device.OnPlatform(20, 10, 10), 5);
    }
    /// <summary>
    /// Called when the tab is selected
    /// </summary>
    internal virtual void PageSelected(){}

  /// <summary>
  /// Called when the tab no longer selected 
  /// </summary>
  internal virtual void PageDeselected(){}

    }
}
