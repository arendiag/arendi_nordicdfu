﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Arendi.BleLibrary;
using Arendi.BleLibrary.Extention;
using RefeneceApp.Model;
using ReferenceApp.Model;
using ReferenceApp.View;
using Arendi.BleLibrary.Local;
using Xamarin.Forms;

namespace ReferenceApp
{
  public class App : Application
	{
    // The screen dimensions of the device
    // see http://stackoverflow.com/questions/24465513/how-to-get-detect-screen-size-in-xamarin-forms
    public static int ScreenWidth;
    public static int ScreenHeight;

    // Inform the user if an exception occurred at startup
    public static string ExceptionMessage = null;

    /// <summary>
    /// The global peripheral manager
    /// </summary>
    internal static ReferencePeripheralManager PeripheralManager { get; private set; }

    /// <summary>
    /// Returns true if bluetooth is enabled. 
    /// The bluetooth state on iOS is "unknown" at startup and changes after a few milli seconds. So we have to check for not "off".
    /// </summary>
    internal static bool BluetoothEnabled { get { return central.BluetoothState != BluetoothState.Off; } }

    internal const string EnableBluetoothMessage = "Please enable the Bluetooth hardware on your device to use this example app";

    private static ICentral central;
    private bool resumeScan;

    public App (int screenWidth, int screenHeight)
    {
      ScreenWidth = screenWidth;
      ScreenHeight = screenHeight;
      central = CentralFactory.GetCentral();
      PeripheralManager = new ReferencePeripheralManager(central);
      // The root page of your application
      MainPage = new AppNavigation(); // property new in 1.3

      resumeScan = false;

      // Notify the user when bluetooth is disabled
      central.BluetoothStateChanged += (sender, args) =>
      {
        if (args.NewStatus == BluetoothState.Off)
        {
          MainPage.DisplayAlert("Bluetooth", EnableBluetoothMessage, "OK");
        }
      };
    }

    /// <summary>
    /// Start the scan for peripherals when the app is resumed.
    /// </summary>
    protected override void OnResume()
    {
      try
      {
        if (resumeScan)
        {
          PeripheralManager.StartScan();
        }
      }
      catch (Exception ex)
      {
        ExceptionMessage = ex.Message;
      }
    }

    /// <summary>
    /// Start the scan for peripherals when the app is started
    /// </summary>
    protected override void OnStart()
    {
      try
      {
        PeripheralManager.StartScan();
      }
      catch (Exception ex)
      {
        ExceptionMessage = ex.Message;
      }
    }

    /// <summary>
    /// Stop the scan for peripherals and disconnect when the app goes to background. 
    /// </summary>
    protected override void OnSleep()
    {
      resumeScan = PeripheralManager.IsScanning;
      if (resumeScan)
      {
        PeripheralManager.StopScan();
      }
      
      var selected = PeripheralManager.Selected;
      if (selected != null && selected.Mode == PeripheralMode.Active)
      {
        selected.Mode = PeripheralMode.Inactive;
      }
      base.OnSleep();
    }
	}
}
