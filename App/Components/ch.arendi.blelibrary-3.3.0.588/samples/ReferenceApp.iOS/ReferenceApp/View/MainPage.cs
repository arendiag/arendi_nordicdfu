using System;
using Arendi.BleLibrary.Extention;
using ReferenceApp.Model;
using Xamarin.Forms;



namespace ReferenceApp.View
{
  /// <summary>
  /// This page allows to establish a connection to a previously selected peripheral and read/write some characteristics
  /// </summary>
  public class MainPage : TabContentPage
  {
    private readonly Label statusValue;
    private readonly Label batteryValue;
    private readonly Label uuidValue;
    private readonly Label rssiValue;
    private readonly Label modelNumberValue;
    private readonly Label firmwareRevisionValue;
    private readonly Label softwareRevisionValue;
    private readonly Label hardwareRevisionValue;
    private readonly Label serialNumberValue;
    private readonly Label manufacturerNameValue;
    private readonly Entry yourPropertyEdit;
    private readonly Button connectButton;


    public MainPage()
    {
      Title = "Connection";
      Icon = "ic_bluetooth_connected.png";
      connectButton = new Button
      {
        Text = "Connect",
        VerticalOptions = LayoutOptions.End,
        HorizontalOptions = LayoutOptions.FillAndExpand,
        BorderColor = Color.Black,
        FontSize = 25,
      };
      connectButton.Clicked += OnConnectButtonClicked;

      var deviceTitle = CreateCaptionLabel("Device");
      var statusTitle = CreateLabel("Status");
      statusValue = CreateLabel("Idle");
      var uuidTitle = CreateLabel("UUID");
      uuidValue = CreateLabel("");
      var rssiTitle = CreateLabel("RSSI Value");
      rssiValue = CreateLabel("---dB");

      var deviceInfoServiceTitle = CreateCaptionLabel("Device Information Service");
      var modelNumberTitle = CreateLabel("Model Number");
      modelNumberValue = CreateLabel(" ");
      var firmwareRevisionTitle = CreateLabel("Firmware Revision");
      firmwareRevisionValue = CreateLabel(" ");
      var softwareRevisionTitle = CreateLabel("Software Revision");
      softwareRevisionValue = CreateLabel(" ");
      var hardwareRevisionTitle = CreateLabel("Hardware Revision");
      hardwareRevisionValue = CreateLabel(" ");
      var serialNumberTitle = CreateLabel("Serial Number");
      serialNumberValue = CreateLabel(" ");
      var manufacturerNameTitle = CreateLabel("Manufacturer Name");
      manufacturerNameValue = CreateLabel(" ");

      var batteryServiceTitle = CreateCaptionLabel("Battery Service");
      var batteryTitle = CreateLabel("Battery level");
      batteryValue = CreateLabel("---%");

      var yourServiceTitle = CreateCaptionLabel("Your Service");
      yourPropertyEdit = CreateEntry("Enter a value for your property");
      yourPropertyEdit.Unfocused += WriteYourProperty;

      var content = new RelativeLayout
      {
        Padding = 20,
      };

      // Device
      content.Children.Add(deviceTitle, Constraint.RelativeToParent((parent) => 0));
      content.Children.Add(statusTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(deviceTitle, (p, v) => v.Height));
      content.Children.Add(statusValue, Constraint.RelativeToParent((p) => Device.OnPlatform(p.Width / 3-20, p.Width / 2, p.Width / 2)), Constraint.RelativeToView(statusTitle, (p, v) => v.Y));
      content.Children.Add(uuidTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(statusTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(uuidValue, Constraint.RelativeToView(statusValue, (p, v) => v.X), Constraint.RelativeToView(uuidTitle, (p, v) => v.Y));
      content.Children.Add(rssiTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(uuidTitle, (p, v) => v.Y+v.Height));
      content.Children.Add(rssiValue, Constraint.RelativeToView(uuidValue, (p, v) => v.X), Constraint.RelativeToView(rssiTitle, (p, v) => v.Y));

      // Device Information Service
      content.Children.Add(deviceInfoServiceTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(rssiTitle, (p, v) => v.Y+v.Height+10));
      content.Children.Add(modelNumberTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(deviceInfoServiceTitle, (p, v) => v.Y+v.Height));
      content.Children.Add(modelNumberValue, Constraint.RelativeToParent((p) => p.Width / 2), Constraint.RelativeToView(modelNumberTitle, (p, v) => v.Y));
      content.Children.Add(firmwareRevisionTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(modelNumberTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(firmwareRevisionValue, Constraint.RelativeToView(modelNumberValue, (p, v) => v.X), Constraint.RelativeToView(firmwareRevisionTitle, (p, v) => v.Y));
      content.Children.Add(softwareRevisionTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(firmwareRevisionTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(softwareRevisionValue, Constraint.RelativeToView(firmwareRevisionValue, (p, v) => v.X), Constraint.RelativeToView(softwareRevisionTitle, (p, v) => v.Y));
      content.Children.Add(hardwareRevisionTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(softwareRevisionTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(hardwareRevisionValue, Constraint.RelativeToView(softwareRevisionValue, (p, v) => v.X), Constraint.RelativeToView(hardwareRevisionTitle, (p, v) => v.Y));
      content.Children.Add(serialNumberTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(hardwareRevisionTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(serialNumberValue, Constraint.RelativeToView(hardwareRevisionValue, (p, v) => v.X), Constraint.RelativeToView(serialNumberTitle, (p, v) => v.Y));
      content.Children.Add(manufacturerNameTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(serialNumberTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(manufacturerNameValue, Constraint.RelativeToView(serialNumberValue, (p, v) => v.X), Constraint.RelativeToView(manufacturerNameTitle, (p, v) => v.Y));

      // Battery Service
      content.Children.Add(batteryServiceTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(manufacturerNameTitle, (p, v) => v.Y + v.Height + 10));
      content.Children.Add(batteryTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(batteryServiceTitle, (p, v) => v.Y + v.Height));
      content.Children.Add(batteryValue, Constraint.RelativeToView(manufacturerNameValue, (p, v) => v.X), Constraint.RelativeToView(batteryTitle, (p, v) => v.Y));

      // Your Service
      content.Children.Add(yourServiceTitle, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(batteryTitle, (p, v) => v.Y + v.Height + 10));
      content.Children.Add(yourPropertyEdit, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(yourServiceTitle, (p, v) => v.Y + v.Height), Constraint.RelativeToParent((p) => p.Width));

      // Connect Button
      content.Children.Add(connectButton, Constraint.RelativeToParent((parent) => 0), Constraint.RelativeToView(yourPropertyEdit, (p, v) => v.Y + v.Height + 10), Constraint.RelativeToParent((p) => p.Width));

      Content = new ScrollView
      {
        VerticalOptions = LayoutOptions.FillAndExpand,
        HorizontalOptions = LayoutOptions.FillAndExpand,
        Content = content,
      };
    }

    protected override void OnAppearing()
    {
      base.OnAppearing ();
    }

    internal override void PageSelected()
    {
      AttachEvents();
      UpdateLabels();
      UpdateConnectButton();
    }

    internal override void PageDeselected()
    {
      DetachEvents();
    }

    protected override void OnDisappearing()
    {
      base.OnDisappearing();
    }

    private void UpdateLabels()
    {
      var selected = App.PeripheralManager.Selected;
      if (selected != null)
      {
        this.Title  = selected.Name;
        statusValue.Text = selected.State.ToString();
        rssiValue.Text = $"{selected.Rssi,3:##0}" + "dB";
        uuidValue.Text = selected.Uuid.ToString();

        if (selected.State == PeripheralState.Ready)
        {
          modelNumberValue.Text = selected.ModelNumber;
          firmwareRevisionValue.Text = selected.FirmwareRevision;
          softwareRevisionValue.Text = selected.SoftwareRevision;
          hardwareRevisionValue.Text = selected.HardwareRevision;
          serialNumberValue.Text = selected.SerialNumber;
          manufacturerNameValue.Text = selected.ManufacturerName;
          if(!string.IsNullOrEmpty(selected.YourProperty))
          {
            yourPropertyEdit.Text = selected.YourProperty;
          }
          batteryValue.Text = $"{selected.BatteryLevel,3:##0}%";
        }
      }
      else
      {
        this.Title = "Connection";
      }
    }

    private void UpdateConnectButton()
    {
      var selected = App.PeripheralManager.Selected;
      if (selected != null)
      {
        connectButton.Text = selected.State != PeripheralState.Idle ? "Disconnect" : "Connect";
      }
    }

    private void AttachEvents()
    {
      var selected = App.PeripheralManager.Selected;
      if (selected != null)
      {
        selected.BatteryLevelChanged += OnBatteryLevelChanged;
        selected.StateChanged += OnStateChanged;
        selected.RssiUpdated += OnRssiUpdated;
      }
    }

    private void DetachEvents()
    {
      var selected = App.PeripheralManager.Selected;
      if (selected != null)
      {
        selected.BatteryLevelChanged -= OnBatteryLevelChanged;
        selected.StateChanged -= OnStateChanged;
        selected.RssiUpdated -= OnRssiUpdated;
      }
    }

    private void OnBatteryLevelChanged(int level)
    {
      Device.BeginInvokeOnMainThread(() => batteryValue.Text = $"{level,3:##0}%");
    }

    private void OnStateChanged(object s, EventArgs e)
    {
      Device.BeginInvokeOnMainThread(() =>
      {
        UpdateLabels();
        UpdateConnectButton();
      });
    }

    private void OnRssiUpdated(object s, RssiUpdatedEventArgs e)
    {
      Device.BeginInvokeOnMainThread(() => rssiValue.Text = $"{e.Rssi,3:##0}" + "dB");
    }

    private Label CreateLabel(string text = "")
    {
      return new Label
      {
        Text = text,
        FontSize = Device.OnPlatform(13,16,16),
        HorizontalOptions = LayoutOptions.Start,
        VerticalOptions = LayoutOptions.CenterAndExpand,
      };
    }

    private Entry CreateEntry(string text = "")
    {
      return new Entry
      {
        Placeholder = text,
        HorizontalOptions = LayoutOptions.StartAndExpand,
        VerticalOptions = LayoutOptions.CenterAndExpand,
      };
    }

    private Label CreateCaptionLabel(string text = "")
    {
      return new Label
      {
        Text = text,
        HorizontalOptions = LayoutOptions.Start,
        VerticalOptions = LayoutOptions.CenterAndExpand,
        FontAttributes = FontAttributes.Bold,
        FontSize = 16.0,
      };
    }

    /// <summary>
    /// Establishes or terminates a connection
    /// </summary>
    /// <param name="s"></param>
    /// <param name="e"></param>
    private void OnConnectButtonClicked(object s, EventArgs e)
    {
      var selected = App.PeripheralManager.Selected;
      if (selected != null)
      {
        if (selected.State == PeripheralState.Idle)
        {
          selected.Establish();
        }
        else
        {
          selected.Teardown();
        }
      }
    }

    /// <summary>
    /// Writes the text from yourPropertyEdit to yourCharacteristic
    /// Change this for your own needs.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void WriteYourProperty(object sender, EventArgs e)
    {
      var selected = App.PeripheralManager.Selected;
      if (selected != null)
      {
        try
        {
          await selected.WriteYourPropertyAsync(yourPropertyEdit.Text);
        }
        catch (Exception ex)
        {
          await DisplayAlert("Data write failed", ex.Message, "Ok");
          return;
        }
        await DisplayAlert("Data write success", "The data write was successful", "Ok");
      }
    }

  }
}

