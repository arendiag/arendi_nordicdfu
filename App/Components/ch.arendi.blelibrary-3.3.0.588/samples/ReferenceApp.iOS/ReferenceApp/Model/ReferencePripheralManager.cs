﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arendi.BleLibrary.Extention;
using Arendi.BleLibrary.Local;
using Arendi.BleLibrary.Remote;
using ReferenceApp.Model;

namespace RefeneceApp.Model
{
  /// <summary>
  /// A specialized PerpheralManager to manage the RefencePeripherals.
  /// </summary>
  public class ReferencePeripheralManager : PeripheralManager<ReferencePeripheral>
  {
    public ReferencePeripheralManager(ICentral central)
      : base(central, (peripheral, advlist, rssi) => new ReferencePeripheral(peripheral))
    {
      // configure the duration a peripheral can be invisible until it is removed from the device list.
      PeripheralInvisibleTimeout = 5;
    }

    private ReferencePeripheral selected;
    /// <summary>
    /// Get/Set the selected device.
    /// </summary>
    public ReferencePeripheral Selected
    {
      get
      {
        return selected;
      }
      set
      {
        // no change ?
        if (value == selected) return;
        // disconnect the "old" selected first
        if (selected != null && selected.State != PeripheralState.Idle)
        {
          selected.Teardown();
        }
        selected = value;
        selected.Mode = PeripheralMode.Manual;
      }
    }

    /// <summary>
    /// Method called when a peripheral has not been seen (advertised) for the configured <see cref="PeripheralManager{T}.PeripheralInvisibleTimeout"/>
    /// time to check if this peripheral can be removed from the list.
    /// </summary>
    /// <param name="peripheral"></param>
    /// <returns></returns>
    protected override bool CheckRemove(ReferencePeripheral peripheral)
    {
      if (peripheral == selected) return (false);
      return true;
    }
  }
}
