using Arendi.BleLibrary.Remote;
using System;
using System.Text;
using System.Threading.Tasks;
using Arendi.BleLibrary;
using Arendi.BleLibrary.Extention;
using Arendi.BleLibrary.Service;
using Arendi.DotNETLibrary.Log;

namespace ReferenceApp.Model
{
  /// <summary>
  /// A specialized EnahancedPeripheral represents your device.
  /// </summary>
  public class ReferencePeripheral : EnhancedPeripheral
  {
    private IPeripheral peripheral;
    private static readonly ILog Log = LogManager.GetLogger("ReferencePeripheral");

    //############################ edit here ###############################################
    // Add your characteristic uuid here
    private const string YourCharacteristicUuid = "00000000-0000-0000-0000-000000000000";
    private ICharacteristic yourCharacteristic;
    //######################################################################################


    public ReferencePeripheral(IPeripheral peripheral) : base(peripheral)
    {
      this.peripheral = peripheral;
    }

    #region events
    internal event Action<int> BatteryLevelChanged = delegate { };
    #endregion

    #region properties
    public string ModelNumber { get; private set; }
    public string FirmwareRevision { get; private set; }
    public string SoftwareRevision { get; private set; }
    public string HardwareRevision { get; private set; }
    public string SerialNumber { get; private set; }
    public string ManufacturerName { get; private set; }

    public string YourProperty { get; private set; }

    private int batteryLevel;
    public int BatteryLevel
    {
      get { return batteryLevel; }
      private set
      {
        batteryLevel = value;
        BatteryLevelChanged(batteryLevel);
      }
    }
    #endregion

    /// <summary>
    /// This method is called when the peripherals state changes from "DiscoverServices" to "Initialize". 
    /// This is a good place to look for characteristics, setup notification and read the initial values.
    /// </summary>
    /// <returns></returns>
    protected override async Task<UpdateSetup> Initialize()
    {
      // DeviceInformation service
      const string deviceInformationServiceUuid = "180A";
      const string deviceInformationCharacteristicManufacturerNameUuid = "2A29";
      const string deviceInformationCharacteristicModelNumberUuid = "2A24";
      const string deviceInformationCharacteristicSerialNumberUuid = "2A25";
      const string deviceInformationCharacteristicHardwareRevisionUuid = "2A27";
      const string deviceInformationCharacteristicFirmwareRevisionUuid = "2A26";
      const string deviceInformationCharacteristicSoftwareRevisionUuid = "2A28";

      //############################ edit here ###############################################
      var uuid = new Uuid(YourCharacteristicUuid.ToLower());
      yourCharacteristic = Peripheral.FindCharacteristic(uuid);
      if (yourCharacteristic != null)
      {
        if ((yourCharacteristic.Property & CharacteristicProperties.Read) == CharacteristicProperties.Read)
        {
          YourProperty = await ReadDataUtf8(yourCharacteristic, "your property");
        }
      }
      else
      {
        Log.Info("The characteristic with the uuid " + YourCharacteristicUuid + " was not found");
      }
      //#####################################333##############################################

      IService deviceInformationService = Peripheral.FindService(deviceInformationServiceUuid.ToLower());
      if (deviceInformationService != null)
      {
        ManufacturerName = await ReadDataUtf8(deviceInformationService.GetCharacteristic(deviceInformationCharacteristicManufacturerNameUuid), "manufacturer name");
        ModelNumber = await ReadDataUtf8(deviceInformationService.GetCharacteristic(deviceInformationCharacteristicModelNumberUuid), "model number");
        SerialNumber = await ReadDataUtf8(deviceInformationService.GetCharacteristic(deviceInformationCharacteristicSerialNumberUuid), "serial number");
        HardwareRevision = await ReadDataUtf8(deviceInformationService.GetCharacteristic(deviceInformationCharacteristicHardwareRevisionUuid), "hardware revision");
        FirmwareRevision = await ReadDataUtf8(deviceInformationService.GetCharacteristic(deviceInformationCharacteristicFirmwareRevisionUuid), "firmware revision");
        SoftwareRevision = await ReadDataUtf8(deviceInformationService.GetCharacteristic(deviceInformationCharacteristicSoftwareRevisionUuid), "software revision");
      }
      else
      {
        Log.Info("DeviceInformation service not found");
      }


      // Battery service
      const string batteryServiceUuid = "180F";
      const string batteryServiceBatteryLevelUuid = "2A19";

      IService batteryService = Peripheral.FindService(batteryServiceUuid.ToLower());
      if (batteryService != null)
      {
        var batCharacteristic = batteryService.GetCharacteristic(batteryServiceBatteryLevelUuid);
        batCharacteristic.NotificationReceived += (sender, args) =>
        {
          BatteryLevel = args.Data[0];
        };
        await batCharacteristic.ChangeNotificationAsync(true);
        BatteryLevel = await ReadDataByte(batCharacteristic, "battery level");
      }
      else
      {
        Log.Info("Battery service not found");
      }

      // done!
      return null;
    }

    //############################ edit here ###############################################
    public async Task WriteYourPropertyAsync(string value)
    {
      if (yourCharacteristic == null)
      {
        throw new NullReferenceException("The characteristic with UUID " + YourCharacteristicUuid + " was not found. Take a look at the ReferencePeripheral.cs source file to find the location where you can enter the characteristic UUID of your Bluetooth LE service of your device.");
      }

      try
      {
        await yourCharacteristic.WriteDataAsync(Encoding.UTF8.GetBytes(value));
      }
      catch (Arendi.BleLibrary.TimeoutException ex)
      {
        Log.Error(ex.Message);
        throw;
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
        throw;
      }
      YourProperty = value;
    }
    //######################################################################################

      /// <summary>
      /// Helper method to read an utf8 encoded string
      /// </summary>
      /// <param name="characteristic"></param>
      /// <param name="logValueName"></param>
      /// <returns></returns>
    private async Task<string> ReadDataUtf8(ICharacteristic characteristic, string logValueName)
    {
      string value = "-";
      if (characteristic != null)
      {
        try
        {
          var data = await Peripheral.ReadDataAsync(characteristic);
          value = Encoding.UTF8.GetString(data, 0, data.Length);
          Log.InfoFormat("{0}: {1}", logValueName, value);
        }
        catch (Exception ex)
        {
          Log.WarnFormat("Read '{0}' failed ({1})", logValueName, ex);
          throw;
        }
      }
      return (value);
    }

    /// <summary>
    /// Helper method to read one byte
    /// </summary>
    /// <param name="characteristic"></param>
    /// <param name="logValueName"></param>
    /// <returns></returns>
    private async Task<byte> ReadDataByte(ICharacteristic characteristic, string logValueName)
    {
      byte value = 0;
      if (characteristic != null)
      {
        try
        {
          value = (await Peripheral.ReadDataAsync(characteristic))[0];
          Log.InfoFormat("{0}: {1}", logValueName, value);
        }
        catch (Exception ex)
        {
          Log.WarnFormat("Read '{0}' failed ({1})", logValueName, ex);
          throw;
        }
      }
      return (value);
    }

  }

}
