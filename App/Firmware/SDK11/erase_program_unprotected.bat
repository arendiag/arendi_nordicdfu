@echo off
echo ---- Erase nRF51 chip ----
nrfjprog --family NRF51 --eraseall
echo.
if %ERRORLEVEL% NEQ 0 goto :error

echo ---- Reset of nRF51 chip to ensure no watchdog is running ----
nrfjprog --family NRF51 --reset
echo.
if %ERRORLEVEL% NEQ 0 goto :error

echo ---- Program nRF51 chip ----
echo ...Softdevice
nrfjprog --family NRF51 --program s130_nrf51_2.0.1_softdevice.hex --verify
echo.
if %ERRORLEVEL% NEQ 0 goto :error

echo ...Bootloader
nrfjprog --family NRF51 --program bootloader.hex --verify
echo.
if %ERRORLEVEL% NEQ 0 goto :error

echo ...Application
nrfjprog --family NRF51 --program application.hex --verify
echo.
if %ERRORLEVEL% NEQ 0 goto :error

echo ...Bootloader (Settings)
nrfjprog --family NRF51 --program ResetBootloaderSettings-nRF51.hex --verify
echo.
if %ERRORLEVEL% NEQ 0 goto :error


:post-programming
echo ---- Reset nRF51 chip to start up with new firmware ----
nrfjprog --family NRF51 --reset
echo.
if %ERRORLEVEL% NEQ 0 goto :error

:success
echo.
echo.
echo SUCCESS - Successfully programmed the nRF51 device
exit /b 0

:error
echo.
echo.
echo ERROR %ERRORLEVEL% - Programming of the nRF51 device failed
exit /b %ERRORLEVEL%