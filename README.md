# Nordic DFU with Xamarin (Android & iOS) #

This repository shows a demo app of the Nordic DFU implementation on Xamarin.

## Components ##

### Arendi.BleLibrary.Service.NordicDFU ###
This library provides the implementation of the Nordic DFU. It may be used free but no official support is provided. 


### Arendi.BleLibrary ###
The library provides Bluetooth Low Energy support for multiple platforms. It handles all the various API's of the different operating
systems and offers a common high level interface for the application.

The BLE library is an official Xamarin component of Arendi AG. The provided version may be used for evaluation, but it has to be licensed for usage in any product.

**Without providing a license key the library will block any BLE operation after two minutes.**

For more information see: [http://ble-library.arendi.ch
](http://ble-library.arendi.ch)

### Arendi.DotNETLibrary ###
This library provides a few basic functions for all of our projects.

The DotNETLibrary is a supporting component of Arendi AG. The provided version may be used for evaluation, but it has to be licensed for usage in any product.


## Getting started ##

### Prepare your Nordic device ###
To test the implemention a device with support for the Nordic DFU is required. In the repository a firmware to test the app is included. Attach a
PCA10028 developer board to your system and start the "App/Firmware/SDK11/erase_program_unprotected.bat" batch file. This will programm your developper board with a
HRS application including a softdevice and bootloader.

### Check out the GIT repository ###
Check out a copy of the git repo for the Arendi Nordic DFU app.

### Build the app with Visual Studio 2015 ###
Open the solution for iOS or Android with your Visual Studio 2015 incl. Xamarin Support. Rebuild the project an deploy it to your device.

NOTE: Building apps with Xamarin for iOS needs a build server, a apple developer account and development certificates.


### Update your Nordic device ###
After the app started you should press the "select peripheral" button to select the device to update. If the test device is not visible it may
have entered the power off state and a reset is required first. If the device is selected the update can be started by pressing "Start Update".


## Who are you talking to? ##

Arendi is a company based in Switzerland.

We provide embedded software for any sized devices:

* Development in Assembler, C and C++ according to well established guide lines
* Design and Development of complex embedded systems
* Design and Development of Userinterfaces (MMI, HMI, GUI)
* Setup of build environments for TDD projects
* Bluetooth Classic and Bluetooth Low Energy
* Time and resource critical real time applications
* RTOS and Embedded Linux based platforms
* Communication software (fieldbus systems, telecom, RF)

BLE competence - More about the BLE competence of Arendi you can find in the [Brochure BLE competence](http://arendi.ch/files/ble-brochure_en_web.pdf).


## Contact ##
E-Mail: [nordicdfu@arendi.ch](mailto:nordicdfu@arendi.ch)

Web: [http://www.arendi.ch](http://www.arendi.ch)